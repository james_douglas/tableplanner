const gulp = require('gulp');

var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var minify = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var ts = require('gulp-typescript');
var concat = require('gulp-concat');
// var commonjs = require('common.js');

function onError(err) {
    console.log(err);
}


/**
 *
 * ===============================
 * SASS Compile
 * ===============================
 *
 */


/*
 ███████  █████  ███████ ███████
 ██      ██   ██ ██      ██
 ███████ ███████ ███████ ███████
      ██ ██   ██      ██      ██
 ███████ ██   ██ ███████ ███████
*/



gulp.task('sass', function () {
    return gulp.src('CSS/scss/toCompile.scss')
        .pipe(sass())
        .pipe(prefix('last 12 versions'))
		.pipe(rename('core.css'))
        .pipe(gulp.dest('CSS'))
        .pipe(plumber({
            errorHandler: onError
        }));
});


gulp.task('sassmin', function () {
    return gulp.src('CSS/scss/toCompile.scss')
        .pipe(sass())
        .pipe(prefix('last 12 versions'))
        .pipe(minify())
        .pipe(rename('core.min.css'))
        .pipe(gulp.dest('CSS'))
        .pipe(plumber({
            errorHandler: onError
        }));
});

/***
 *
 *===============================
 * Javascript CONCAT
 *===============================
 *
 */
//
//
// gulp.task('scripts', function() {
//     return gulp.src(['./wp-content/plugins/kloc-membership/dev/vendors/bootstrap/js/*.js', 'wp-content/plugins/kloc-membership/dev/vendors/parsley/parsely.js'])
//         .pipe(concat('plugin-admin-bootstrap.js'))
//         .pipe(gulp.dest('./wp-content/plugins/kloc-membership/js/'));
// });


/*
██     ██  █████  ████████  ██████ ██   ██
██     ██ ██   ██    ██    ██      ██   ██
██  █  ██ ███████    ██    ██      ███████
██ ███ ██ ██   ██    ██    ██      ██   ██
 ███ ███  ██   ██    ██     ██████ ██   ██
*/




/**
 * Watch SASS files
 */

gulp.task('sass:watch', function () {
    gulp.watch([
        'CSS/scss/*.scss',
        'CSS/*.scss'

    ], [
        'sass',
        'sassmin'
    ]);
});


gulp.task('default', ['sass:watch']);
