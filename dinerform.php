<div class="dinerform-window">
	<div class="dinerform-window-inner">
		<div class="dinerform-window-container">
			<div class="dinerform-window-close"><i class="fa fa-times" aria-hidden="true"></i></div>
			<div class="dinerform-window-message">
				<h2>Add a diner </h2>
				<form class="dinerform-form" action="">
					<label for="name" >Name:</label>
					<textarea name="name" placeholder="Name"></textarea>
					<h3>Add guest(s)</h3>
					<a href="#" class="dinerform-form-addguest">Add <i class="fa fa-plus" aria-hidden="true"></i></a>
					<div class="dinerform-form-guests"></div>
					<input type="submit" value="Submit">
				</form>
			</div>
		</div>
	</div>
</div>
