<nav>
	<ul class="menu-container">
		<li class="menu-parent">
			<a href="#">File <i class="fa fa-caret-down" aria-hidden="true"></i></a>
			<ul class="menu-child">
				<li class="menu-grandparent">
					<a href="#" class="not-supported">Open</a>
				</li>
				<li class="menu-grandparent">
					<a href="#" class="not-supported">Save</a>
				</li>

				<li class="menu-grandparent">
					<a href="#">Import Event <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="menu-grandchild">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<span>Events</span>
								<ul class="flex-grid events-list"> </ul>
							</div>
						</div>
					</div>
				</li>
				<li class="menu-grandparent">
					<a href="#" class="print-canvas">Print</a>
				</li>
			</ul>
		</li>
		<li class="menu-parent">
			<a href="#">Insert <i class="fa fa-caret-down" aria-hidden="true"></i></a>
			<ul class="menu-child">
				<li class="menu-grandparent">
					<a href="#">Table <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="menu-grandchild">
						<div class="row">
							<div class="col-lg-2 col-md-2">
								<span>Rectangular</span>
								<ul>
									<li><a href="#" class="create-table" data-tabletype="RectangleTable">Rectangle</a></li>
									<li><a href="#" class="create-table" data-tabletype="RectangleTableTopTable">Rectangle top-table</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Diagonal clockwise</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Diagonal counter-clockwise</a></li>
								</ul>
							</div>
							<div class="col-lg-2 col-md-2">
								<span>Circular</span>
								<ul>
									<li><a href="#" class="create-table" data-tabletype="CircleTable">Circle</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Oval horizontal</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Oval vertical</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-2">
								<span>Sprigs</span>
								<ul>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Horizontal with two sprigs</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Horizontal with two sprigs (chamfered)</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Horizontal with two sprigs (rounded)</a></li>
									<li><a href="#" class="create-table" data-tabletype="HorseshoeTable">Horse-shoe with 2 sprigs</a></li>
									<li><a href="#" class="create-table not-supported" data-tabletype="">Vertical sprig</a></li>
								</ul>
							<div>
						</div>
					</div>
				</li>
				<li class="menu-grandparent">
					<a href="#">Object <i class="fa fa-caret-down" aria-hidden="true"></i></a>
					<div class="menu-grandchild">
						<div class="row">
							<div class="col-lg-2 col-md-2">
								<span>Graphics</span>
								<ul>
									<li><a href="#" class="not-supported" data-tabletype="">Text</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Line</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Rectangle</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Circle</a></li>
									<li><a href="#" class="import-image" data-tabletype="image">Image</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Symbol</a></li>
								</ul>
							</div>
							<div class="col-lg-2 col-md-2">
								<span>Example</span>
								<ul>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-2">
								<span>Example</span>
								<ul>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
									<li><a href="#" class="not-supported" data-tabletype="">Example</a></li>
								</ul>
							<div>
						</div>
					</div>
				</li>
				<li class="menu-grandparent">
					<a href="#" class=" not-supported">Layout Page</a>
				</li>
				<li class="menu-grandparent">
					<a href="#" class="manually-add-diner">Diners</a>
				</li>
			</ul>
		</li>
		<li class="menu-parent">
			<a href="#">Fonts <i class="fa fa-caret-down" aria-hidden="true"></i></a>
			<div class="menu-child menu-manualclose">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<span>Global font properties</span>
						<div class="font-item">
							<span class="menu-child-subtitle">Font Size</span>
							<input class="global-font-input global-fontsize" type="text" name="fontsize" value="" data-for="font-size" data-textattr="fontsize">
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle">Line-height</span>
							<input class="global-font-input global-lineheight" type="text" name="fontsize" value="" data-for="font-lineheight" data-textattr="lineheight">
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle">Opacity</span>
							<select class="global-opacity" data-textattr="opacity">
								<option value="0" selected>0%</option>
								<option value="0.1" selected>10%</option>
								<option value="0.2" selected>20%</option>
								<option value="0.3" selected>30%</option>
								<option value="0.4" selected>40%</option>
								<option value="0.5" selected>50%</option>
								<option value="0.6" selected>60%</option>
								<option value="0.7" selected>70%</option>
								<option value="0.8" selected>80%</option>
								<option value="0.9" selected>90%</option>
								<option value="1" selected>100%</option>
							</select>
						</div>



					</div>
					<div class="col-lg-4 col-md-4">
						<span>&nbsp;</span>

						<div class="font-item">
							<span class="menu-child-subtitle">Font Family</span>
							<select class="canvas-font-family global-fontfamily" data-textattr="fontfamily">
								<option value="arial" selected="">Arial</option>
								<option value="helvetica">Helvetica</option>
								<option value="myriad pro">Myriad Pro</option>
								<option value="delicious">Delicious</option>
								<option value="verdana">Verdana</option>
								<option value="georgia">Georgia</option>
								<option value="courier">Courier</option>
								<option value="comic sans ms">Comic Sans MS</option>
								<option value="impact">Impact</option>
								<option value="monaco">Monaco</option>
								<option value="optima">Optima</option>
								<option value="hoefler text">Hoefler Text</option>
								<option value="plaster">Plaster</option>
								<option value="engagement">Engagement</option>
				            </select>
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle">Font Style</span>
							<select class="canvas-font-style global-font-style" data-textattr="fontstyle">
								<option value="normal" selected="">Normal</option>
								<option value="italic">Italic</option>
								<option value="bold">Bold</option>
								<option value="linethrough">Line-through</option>
								<option value="underline">Underline</option>
								<option value="overline">Overline</option>
				            </select>
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle">Stroke Width</span>
							<input class="global-font-input global-strokewidth" type="text" name="fontsize" value="" data-for="font-strokewidth" data-textattr="strokewidth">
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<span>&nbsp;</span>
						<div class="font-item">
							<span class="menu-child-subtitle menu-child-colour">Font Colour</span>
							<input class="canvas-font-colour global-fontcolour" data-textattr="fontcolour" value="#000000" type="color"/>
							<div class="color-reset" data-textattr="fontcolour"><i class="fa fa-times" aria-hidden="true"></i></div>
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle menu-child-colour">Background</span>
							<input class="canvas-font-backgroundcolour global-fontbackground" data-textattr="fontbackground" value="#ffffff" type="color"/>
							<div class="color-reset" data-textattr="fontbackground"><i class="fa fa-times" aria-hidden="true"></i></div>
						</div>

						<div class="font-item">
							<span class="menu-child-subtitle menu-child-colour">Stroke Colour</span>
							<input class="canvas-font-strokecolour global-fontstroke" data-textattr="strokecolour" value="#ffffff" type="color"/>
							<div class="color-reset" data-textattr="strokecolour"><i class="fa fa-times" aria-hidden="true"></i></div>
						</div>
					</div>
					<div class="col-lg-12">
						<a href="#" class="update-global-font">Update <i class="fa fa-refresh" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>
		</li>
		<li class="menu-parent">
			<a href="#" class=" not-supported">Colours</a>
		</li>
		<li class="menu-parent">
			<a href="#" class=" not-supported">Settings <i class="fa fa-caret-down" aria-hidden="true"></i></a>
			<ul class="menu-child">
				<li class="menu-grandparent">
					<a href="#" class="">1</a>
				</li>
				<li class="menu-grandparent">
					<a href="#" class="">2</a>

				</li>
			</ul>

		</li>
	</ul>
</nav>
<!-- <i class="fa fa-microchip" aria-hidden="true"></i> -->
