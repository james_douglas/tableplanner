<meta charset="utf-8">
<?php include 'site_config.php'; ?>

<title><?php echo $site_title;?></title>
<meta name="description" content="<?php echo $site_description;?>">
<meta name="author" content="<?php echo $site_title;?>">
<meta name="keywords" content="<?php echo $site_keywords;?>">
<meta name="format-detection" content="telephone=no">

<link rel="canonical" href="<?php echo $site_url;?>">
<meta property="og:locale" content="en_US">
<meta property="og:type" content="website">
<meta property="og:title" content="<?php echo $site_title;?>">
<meta property="og:description" content="<?php echo $site_description;?>">
<meta property="og:url" content="<?php echo $site_url;?>">
<meta property="og:site_name" content="<?php echo $site_title;?>">
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="<?php echo $site_description;?>">
<meta name="twitter:title" content="<?php echo $site_title;?>">

<!-- Mobile Viewport -->
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi" />

<!-- SITE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet">

<!-- FONT AWESOME -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="CSS/bootstrap.min.css">
<link rel="stylesheet" href="CSS/core.css">
<link rel="stylesheet" href="CSS/upscale.css">
<link rel="stylesheet" href="CSS/animation.css">
<link rel="stylesheet" href="CSS/jquery-ui.min.css">
<link rel="stylesheet" type="text/css" media="print" href="CSS/print.css" />


<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="JS/core/bootstrap.min.js"></script>
<script src="JS/controller/core.js"></script>
<script src="JS/core/jquery-ui.min.js"></script>
