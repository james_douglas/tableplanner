<div id="rightclick-font-context">
	<div class="rightclick-font-context-container">
		<div class="rightclick-font-context-inner">
			<div class="rightclick-font-context-close">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 context-font-properties">
					<h2>Diner Properties</h2>
					<div class="font-item">
						<span class="menu-child-subtitle">Font Size</span>
						<input class="single-font-input single-fontsize" type="text" name="fontsize" value="" data-for="font-size" data-textattr="fontsize">
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Line-height</span>
						<input class="single-font-input single-lineheight" type="text" name="fontsize" value="" data-for="font-lineheight" data-textattr="lineheight">
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Opacity</span>
						<select class="single-opacity rightclick-font-context" data-textattr="opacity">
							<option value="0" selected>0%</option>
							<option value="0.1" selected>10%</option>
							<option value="0.2" selected>20%</option>
							<option value="0.3" selected>30%</option>
							<option value="0.4" selected>40%</option>
							<option value="0.5" selected>50%</option>
							<option value="0.6" selected>60%</option>
							<option value="0.7" selected>70%</option>
							<option value="0.8" selected>80%</option>
							<option value="0.9" selected>90%</option>
							<option value="1" selected>100%</option>
						</select>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Font Family</span>
						<select class="canvas-font-family single-fontfamily rightclick-font-context" data-textattr="fontfamily">
							<option value="arial" selected>Arial</option>
							<option value="helvetica">Helvetica</option>
							<option value="myriad pro">Myriad Pro</option>
							<option value="delicious">Delicious</option>
							<option value="verdana">Verdana</option>
							<option value="georgia">Georgia</option>
							<option value="courier">Courier</option>
							<option value="comic sans ms">Comic Sans MS</option>
							<option value="impact">Impact</option>
							<option value="monaco">Monaco</option>
							<option value="optima">Optima</option>
							<option value="hoefler text">Hoefler Text</option>
							<option value="plaster">Plaster</option>
							<option value="engagement">Engagement</option>
						</select>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Font Style</span>
						<select class="canvas-font-input single-fontstyle rightclick-font-context" data-textattr="fontstyle">
							<option value="" selected>Normal</option>
							<option value="italic">Italic</option>
							<option value="bold">Bold</option>
							<option value="linethrough">Line-through</option>
							<option value="underline">Underline</option>
							<option value="overline">Overline</option>
						</select>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Stroke Width</span>
						<input class="single-font-input single-strokewidth" type="text" name="fontsize" value="" data-for="font-strokewidth" data-textattr="strokewidth">
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle menu-child-colour">Stroke Colour</span>
						<input class="canvas-font-strokecolour single-font-strokecolour rightclick-font-context" data-textattr="strokecolour" type="color" value="#ffffff"/ >
						<div class="color-reset" data-textattr="strokecolour"><i class="fa fa-times" aria-hidden="true"></i></div>
						<div class="font-example"></div>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle menu-child-colour">Font Colour</span>
						<input class="canvas-font-colour single-font-colour rightclick-font-context" data-textattr="fontcolour" value="#000000" type="color"/>
						<div class="color-reset" data-textattr="fontcolour"><i class="fa fa-times" aria-hidden="true"></i></div>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle menu-child-colour">Background</span>
						<input class="canvas-font-backgroundcolour single-font-backgroundcolour rightclick-font-context" data-textattr="fontbackground" value="#ffffff" type="color"/>
						<div class="color-reset" data-textattr="fontbackground"><i class="fa fa-times" aria-hidden="true"></i></div>
					</div>


					<a href="#" class="update-single-font">Update <i class="fa fa-refresh" aria-hidden="true"></i></a>
					<a href="#" class="reset-single-styling">Reset <i class="fa fa-times" aria-hidden="true"></i></a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 context-table-properties">
					<h2>Table Properties</h2>
					<div class="font-item not-supported">
						<span class="menu-child-subtitle">Table Width</span>
						<input class="single-object-input canvas-object-tablewidth" type="text" name="fontsize" value="" data-for="tablewidth" data-textattr="tablewidth">
					</div>
					<div class="font-item not-supported">
						<span class="menu-child-subtitle">Table Height</span>
						<input class="single-object-input canvas-object-tableheight" type="text" name="fontsize" value="" data-for="tableheight" data-textattr="tableheight">
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle">Opacity</span>
						<select class="single-object rightclick-context-object canvas-object-opacity" data-textattr="obj-opacity">
							<option value="0" selected>0%</option>
							<option value="0.1" selected>10%</option>
							<option value="0.2" selected>20%</option>
							<option value="0.3" selected>30%</option>
							<option value="0.4" selected>40%</option>
							<option value="0.5" selected>50%</option>
							<option value="0.6" selected>60%</option>
							<option value="0.7" selected>70%</option>
							<option value="0.8" selected>80%</option>
							<option value="0.9" selected>90%</option>
							<option value="1" selected>100%</option>
						</select>
					</div>

					<div class="font-item not-supported">
						<span class="menu-child-subtitle">Stroke Width</span>
						<input class="single-object-input canvas-object-strokewidth" type="text" name="fontsize" value="" data-for="strokewidth" data-textattr="strokewidth">
					</div>

					<div class="font-item not-supported">
						<span class="menu-child-subtitle">Placeholder Font Size</span>
						<input class="single-object-input canvas-object-placeholdersize" type="text" name="placeholdersize" value="" data-for="placeholdersize" data-textattr="placeholdersize">
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle menu-child-colour">Stroke Colour</span>
						<input class="canvas-object-strokecolour" data-textattr="strokecolour" value="#cccccc" type="color"/>
						<div class="color-reset object-colour-reset" data-textattr="strokecolour"><i class="fa fa-times" aria-hidden="true"></i></div>
					</div>

					<div class="font-item">
						<span class="menu-child-subtitle menu-child-colour">Background</span>
						<input class="canvas-object-backgroundcolour" data-textattr="fontbackground" value="#cccccc" type="color"/>
						<div class="color-reset object-colour-reset" data-textattr="fontbackground"><i class="fa fa-times" aria-hidden="true"></i></div>
					</div>
					<div class="font-item">
						<p>&nbsp;</p>
					</div>
					<div class="font-item">
						<p>&nbsp;</p>
					</div>

					<a href="#" class="update-table">Update <i class="fa fa-refresh" aria-hidden="true"></i></a>
					<a href="#" class="reset-table">Reset <i class="fa fa-times" aria-hidden="true"></i></a>
					<a href="#" class="delete-table">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
				</div>
			</div>

		</div>
	</div>
</div>
