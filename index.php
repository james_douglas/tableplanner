<!doctype html>
<html lang="en" moznomarginboxes mozdisallowselectionprint>
  <head>
	<?php include 'config.php'; ?>
    <?php include 'header.php'; ?>
  </head>
	<body>
		<header>
	  		<?php include 'navigation.php'; ?>
	    </header>

		<!--- SECTION --->
		<div class="tp-container">
			<div class="tp-columns">
				<div class="tp-pagetabs-tabcontent-container">
					<div class="tp-pagetabs-container">
						<div class="tp-pagetabs-tab tp-pagetabs-active" data-tab="1">
							Planner 1
						</div>
						<div class="tp-pagetabs-tab" data-tab="2">
							<i class="fa fa-plus" aria-hidden="true"></i>
						</div>
					</div>
					<div class="tp-pagetabs-tabcontent tp-pagetabs-tabcontent-active" data-tab="1">
						<canvas id="canvas" width="1000px" height="600"></canvas>
					</div>
					<div class="tp-pagetabs-tabcontent" data-tab="2">
						<!-- <canvas id="canvas" width="1000px" height="600"></canvas> -->
					</div>
				</div>
				<div class="tp-diners-container">
					<span class="event-diners-close"><i class="fa fa-times" aria-hidden="true"></i></span>
					<span class="event-diners-title">New Event</span>
					<ul class="tp-diners-list"></ul>
					<div class="tp-diners-options">
						<a href="#" class="tp-diners-options-importall">Add all <i class="fa fa-plus" aria-hidden="true"></i></a>
					</div>
				</div>
			</div>

		</div>

		<?php include('contextmenu.php');?>

		<div class="notification-window">
			<div class="notification-window-inner">
				<div class="notification-window-container">
					<div class="notification-window-close"><i class="fa fa-times" aria-hidden="true"></i></div>
					<div class="notification-window-message"></div>
				</div>
			</div>
		</div>
		<div class="imageuploader-window">
			<div class="imageuploader-window-inner">
				<div class="imageuploader-window-container">
					<div class="imageuploader-window-close"><i class="fa fa-times" aria-hidden="true"></i></div>
					<div class="imageuploader-window-message">
						<h2>Import an Image</h2>

						<input type="file" name="import-image-input" id="import-image-input" class="import-image-filepath" accept="image/*"/>
						<label for="import-image-input">here</label>

						<a href="#" class="import-image-file">Import</a>
					</div>
				</div>
			</div>
		</div>

		<?php include('dinerform.php'); ?>

		<script src="JS/core/fabric/dist/fabric.js"></script>
		<!-- <script src="JS/fabric_kloc_rect.js"></script> -->
		<script src="JS/controller/fabric_kloc_objext.js"></script>
		<!-- <script src="JS/fabric_kloc_boundary.js"></script> -->
		<script src="JS/controller/fabric_kloc_images.js"></script>
		<script src="JS/controller/fabric_kloc_menu.js"></script>
		<script src="JS/controller/fabric_kloc_groups.js"></script>
		<script src="JS/controller/fabric_kloc_objectcontext.js"></script>
		<script src="JS/controller/fabric_kloc_font.js"></script>
		<script src="JS/controller/fabric_kloc_diners.js"></script>
		<script src="JS/controller/fabric_kloc_events.js"></script>
		<script src="JS/tables/fabric_kloc_circle.js"></script>
		<script src="JS/tables/fabric_kloc_horseshoe.js"></script>
		<script src="JS/tables/fabric_kloc_rectangle.js"></script>
		<script src="JS/tables/fabric_kloc_rectangle_toptable.js"></script>
		<script src="JS/controller/fabric_kloc_core.js"></script>



	    <?php include 'footer.php'; ?>

	</body>
</html>
