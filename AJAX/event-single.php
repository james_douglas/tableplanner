<?php
$eventID = $_POST['eventID'];

$myArr = array(
	"eventID" => $eventID,
	"event" =>  array(
		"event_name" 		=> "Corporate Event 1",
		"event_date" 		=> "01/01/2017",
		"event_id" 			=> "1",
		"diners" 			=>  array(
			0	=>  array(
				"name" 		=> "Eddard Stark",
				"subtitle" 	=> "King in the North",
				"guests"	=> array(
					0	=>  array(
		 				"name"		=> "Sansa Stark",
		 				"subtitle"	=> ""
		 			),
		 			1	=>  array(
		 				"name"		=> "Brandon Stark",
		 				"subtitle"	=> "Stark"
		 			),
		 			2	=>  array(
		 				"name"		=> "Rickon Stark",
		 				"subtitle"	=> "That Stark everyone forgets about"
		 			),
		 			3	=>  array(
		 			  "name"		=> "Arya Stark",
		 			  "subtitle"	=> "A girl"
				  	),
		 			4	=>  array(
		 			  "name"		=> "Jon Snow",
		 			  "subtitle"	=> "Knows nothing"
				  	)
				)
			),
			1	=>  array(
				"name" 		=> "Tony Stark",
				"subtitle" 	=> "Iron Man"
			),
			2	=>  array(
				"name" => "Daenerys Targaryen",
		  	  	"subtitle" => "Daenerys Stormborn of the House Targaryen, First of Her Name, the Unburnt, Queen of the Andals and the First Men, Khaleesi of the Great Grass Sea, Breaker of Chains, and Mother of Dragons"
		  	),
			3	=>  array(
				"name" => "Petyr Baelish",
		  	  	"subtitle" => "Captain weird accent"
			),
			4	=>  array(
				"name" => "Jorah Mormont",
		  	  	"subtitle" => "Jorah the Explorer"
			),
			5	=>  array(
			  	"name" => "Theon Grejoy",
			  	"subtitle" => "Walls Sausages"
		  	),
			6	=>  array(
			  	"name" => "Sandor Clegane",
			  	"subtitle" => "Eater of chicken"
		  	),
			7	=>  array(
			  	"name" => "Hodor",
			  	"subtitle" => "Hold the door"
		  	),
			8	=>  array(
				"name"		=> "Splinter",
				"subtitle"	=> "Master",
				"guests"	=> array(
					0	=>  array(
					   "name" => "Michelangelo",
					   "subtitle" => "Turtle"
				   	),
					1	=>  array(
					  "name" => "Leonardo",
					  "subtitle" => "Turtle"
				  	),
					2	=>  array(
					  "name" => "Donatello",
					  "subtitle" => "Turtle"
				  	),
					3	=>  array(
					  "name" => "Raphael",
					  "subtitle" => "Turtle"
				  	)
				)
			),
			9	=>  array(
			  "name" => "Tywin Lannister",
			  "subtitle" => ""
			),
			10	=>  array(
			  "name" => "Joanna Lannister",
			  "subtitle" => ""
			),
			11	=>  array(
			  "name" => "Tyrion Lannister",
			  "subtitle" => ""
			),
			12	=>  array(
			  "name" => "Jaime Lannister",
			  "subtitle" => ""
			),
			13	=>  array(
			  "name" => "Cersei Lannister",
			  "subtitle" => ""
			),
			14	=>  array(
			  "name" => "Joffrey Baratheon",
			  "subtitle" => ""
			),
			15	=>  array(
			  "name" => "Robert Baratheon",
			  "subtitle" => ""
			),
			16	=>  array(
			  "name" => "Diner 16",
			  "subtitle" => ""
			),
			17	=>  array(
			  "name" => "Diner 17",
			  "subtitle" => ""
			),
			18	=>  array(
			  "name" => "Diner 18",
			  "subtitle" => ""
			),
			19	=>  array(
			  "name" => "Diner 19",
			  "subtitle" => ""
			),
			20	=>  array(
			  "name" => "Diner 20",
			  "subtitle" => ""
			),
			21	=>  array(
			  "name" => "Diner 21",
			  "subtitle" => ""
			),
			22	=>  array(
			  "name" => "Diner 22",
			  "subtitle" => ""
			),
			23	=>  array(
			  "name" => "Diner 23",
			  "subtitle" => ""
			),
			24	=>  array(
			  "name" => "Diner 24",
			  "subtitle" => ""
			),
			25	=>  array(
			  "name" => "Diner 25",
			  "subtitle" => ""
			),
			26	=>  array(
			  "name" => "Diner 26",
			  "subtitle" => ""
			),
			27	=>  array(
			  "name" => "Diner 27",
			  "subtitle" => ""
			),
			28	=>  array(
			  "name" => "Diner 28",
			  "subtitle" => ""
			),
			29	=>  array(
			  "name" => "Diner 29",
			  "subtitle" => ""
			),
			30	=>  array(
			  "name" => "Diner 30",
			  "subtitle" => ""
			),
			31	=>  array(
			  "name" => "Diner 31",
			  "subtitle" => ""
			),
			32	=>  array(
			  "name" => "Diner 32",
			  "subtitle" => ""
			),
			33	=>  array(
			  "name" => "Diner 33",
			  "subtitle" => ""
			),
			34	=>  array(
			  "name" => "Diner 34",
			  "subtitle" => ""
			),
			35	=>  array(
			  "name" => "Diner 35",
			  "subtitle" => ""
			),
			36	=>  array(
			  "name" => "Diner 36",
			  "subtitle" => ""
			),
			37	=>  array(
			  "name" => "Diner 37",
			  "subtitle" => ""
			),
			38	=>  array(
			  "name" => "Diner 38",
			  "subtitle" => ""
			),
			39	=>  array(
			  "name" => "Diner 39",
			  "subtitle" => ""
			),
			40	=>  array(
			  "name" => "Diner 40",
			  "subtitle" => ""
			),
			41	=>  array(
			  "name" => "Diner 41",
			  "subtitle" => ""
			),
			42	=>  array(
			  "name" => "Diner 42",
			  "subtitle" => ""
			),
			43	=>  array(
			  "name" => "Diner 43",
			  "subtitle" => ""
			),
			44	=>  array(
			  "name" => "Diner 44",
			  "subtitle" => ""
			),
			45	=>  array(
			  "name" => "Diner 45",
			  "subtitle" => ""
			),
			46	=>  array(
			  "name" => "Diner 46",
			  "subtitle" => ""
			),
			47	=>  array(
			  "name" => "Diner 47",
			  "subtitle" => ""
			),
			48	=>  array(
			  "name" => "Diner 48",
			  "subtitle" => ""
			),
			49	=>  array(
			  "name" => "Diner 49",
			  "subtitle" => ""
			),
			50	=>  array(
			  "name" => "Diner 50",
			  "subtitle" => ""
			),
			51	=>  array(
			  "name" => "Diner 51",
			  "subtitle" => ""
			),
			52	=>  array(
			  "name" => "Diner 52",
			  "subtitle" => ""
			),
			53	=>  array(
			  "name" => "Diner 53",
			  "subtitle" => ""
			),
			54	=>  array(
			  "name" => "Diner 54",
			  "subtitle" => ""
			),
			55	=>  array(
			  "name" => "Diner 55",
			  "subtitle" => ""
			),
			56	=>  array(
			  "name" => "Diner 56",
			  "subtitle" => ""
			),
			57	=>  array(
			  "name" => "Diner 57",
			  "subtitle" => ""
			),
			58	=>  array(
			  "name" => "Diner 58",
			  "subtitle" => ""
			),
			59	=>  array(
			  "name" => "Diner 59",
			  "subtitle" => ""
			),
			60	=>  array(
			  "name" => "Diner 60",
			  "subtitle" => ""
			),
			61	=>  array(
			  "name" => "Diner 61",
			  "subtitle" => ""
			),
			62	=>  array(
			  "name" => "Diner 62",
			  "subtitle" => ""
			),
			63	=>  array(
			  "name" => "Diner 63",
			  "subtitle" => ""
			),
			64	=>  array(
			  "name" => "Diner 64",
			  "subtitle" => ""
			),
			65	=>  array(
			  "name" => "Diner 65",
			  "subtitle" => ""
			),
			66	=>  array(
			  "name" => "Diner 66",
			  "subtitle" => ""
			),
			67	=>  array(
			  "name" => "Diner 67",
			  "subtitle" => ""
			),
			68	=>  array(
			  "name" => "Diner 68",
			  "subtitle" => ""
			),
			69	=>  array(
			  "name" => "Diner 69",
			  "subtitle" => ""
			),
			70	=>  array(
			  "name" => "Diner 70",
			  "subtitle" => ""
			),
			71	=>  array(
			  "name" => "Diner 71",
			  "subtitle" => ""
			),
			72	=>  array(
			  "name" => "Diner 72",
			  "subtitle" => ""
			),
			73	=>  array(
			  "name" => "Diner 73",
			  "subtitle" => ""
			),
			74	=>  array(
			  "name" => "Diner 74",
			  "subtitle" => ""
			),
			75	=>  array(
			  "name" => "Diner 75",
			  "subtitle" => ""
			),
			76	=>  array(
			  "name" => "Diner 76",
			  "subtitle" => ""
			),
			77	=>  array(
			  "name" => "Diner 77",
			  "subtitle" => ""
			),
			78	=>  array(
			  "name" => "Diner 78",
			  "subtitle" => ""
			),
			79	=>  array(
			  "name" => "Diner 79",
			  "subtitle" => ""
			),
			80	=>  array(
			  "name" => "Diner 80",
			  "subtitle" => ""
			),
			81	=>  array(
			  "name" => "Diner 81",
			  "subtitle" => ""
			),
			82	=>  array(
			  "name" => "Diner 82",
			  "subtitle" => ""
			),
			83	=>  array(
			  "name" => "Diner 83",
			  "subtitle" => ""
			),
			84	=>  array(
			  "name" => "Diner 84",
			  "subtitle" => ""
			),
			85	=>  array(
			  "name" => "Diner 85",
			  "subtitle" => ""
			),
			86	=>  array(
			  "name" => "Diner 86",
			  "subtitle" => ""
			),
			87	=>  array(
			  "name" => "Diner 87",
			  "subtitle" => ""
			),
			88	=>  array(
			  "name" => "Diner 88",
			  "subtitle" => ""
			),
			89	=>  array(
			  "name" => "Diner 89",
			  "subtitle" => ""
			),
			90	=>  array(
			  "name" => "Diner 90",
			  "subtitle" => ""
			),
			91	=>  array(
			  "name" => "Diner 91",
			  "subtitle" => ""
			),
			92	=>  array(
			  "name" => "Diner 92",
			  "subtitle" => ""
			),
			93	=>  array(
			  "name" => "Diner 93",
			  "subtitle" => ""
			),
			94	=>  array(
			  "name" => "Diner 94",
			  "subtitle" => ""
			),
			95	=>  array(
			  "name" => "Diner 95",
			  "subtitle" => ""
			),
			96	=>  array(
			  "name" => "Diner 96",
			  "subtitle" => ""
			),
			97	=>  array(
			  "name" => "Diner 97",
			  "subtitle" => ""
			),
			98	=>  array(
			  "name" => "Diner 98",
			  "subtitle" => ""
			),
			99	=>  array(
			  "name" => "Diner 99",
			  "subtitle" => ""
			),
			100	=>  array(
			  "name" => "Diner 100",
			  "subtitle" => ""
			)
		)
	)
);

$myJSON = json_encode($myArr);

echo $myJSON;
?>
