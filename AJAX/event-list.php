<?php

$myArr = array(
	"events" =>  array(
		0 =>  array(
			"event_name" 		=> "Corporate Event 1",
			"event_date" 		=> "01/01/2017",
			"event_id" 			=> "1",
			"event_location" 	=> "The Moon"
		),
		1 =>  array(
			"event_name" 		=> "Corporate Event 2",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		2 =>  array(
			"event_name" 		=> "Corporate Event 3",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		3 =>  array(
			"event_name" 		=> "Corporate Event 3.5",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		4 =>  array(
			"event_name" 		=> "Corporate Event 4",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		5 =>  array(
			"event_name" 		=> "Corporate Event 5",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		6 =>  array(
			"event_name" 		=> "Corporate Event 6",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		7 =>  array(
			"event_name" 		=> "Corporate Event 7",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		8 =>  array(
			"event_name" 		=> "Corporate Event 8",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		),
		9 =>  array(
			"event_name" 		=> "Corporate Event 9",
			"event_date" 		=> "02/01/2017",
			"event_id" 			=> "2",
			"event_location" 	=> "The Moon"
		)
	)
);

$myJSON = json_encode($myArr);

echo $myJSON;
?>
