
/*
██████  ███████  ██████ ████████  █████  ███    ██  ██████  ██      ███████ ████████  █████  ██████  ██      ███████
██   ██ ██      ██         ██    ██   ██ ████   ██ ██       ██      ██         ██    ██   ██ ██   ██ ██      ██
██████  █████   ██         ██    ███████ ██ ██  ██ ██   ███ ██      █████      ██    ███████ ██████  ██      █████
██   ██ ██      ██         ██    ██   ██ ██  ██ ██ ██    ██ ██      ██         ██    ██   ██ ██   ██ ██      ██
██   ██ ███████  ██████    ██    ██   ██ ██   ████  ██████  ███████ ███████    ██    ██   ██ ██████  ███████ ███████
KLOC 2017
DEV: James Douglas
*/



function RectangleTable(dimensions,coords) {
	this.$dimensions = dimensions; //Passed in dimensions
	this.$coords = coords; //Passed in coordinates
	this.height = this.$dimensions.height;
	this.width = this.$dimensions.width;
	this.top = this.$coords.top;
	this.left = this.$coords.left;
	this.$boundaries = []; //Array of all created objects, each with name
	this.$group = {}; //The group as an object
	this.$ghost = {};


	this.init();
	this.createParentAndBoundaries();
}

RectangleTable.prototype.init = function(){
	var self = this;
}

RectangleTable.prototype.reshuffleBoundaryItems = function(boundary,parentObj){
	var self = this;
	console.log('RECTANGLE | Reshuffling boundary items');
	var textItemsToShuffle = boundary.KLOCSeated;
	var boundaryOrientation = boundary.KLOCOrientation;

	for (x=0; x<boundary.KLOCDinerPosition.length; x++){
		canvas.remove(boundary.KLOCDinerPosition[x]);
	}

	for (i=0; i<textItemsToShuffle.length; i++){
		var halfTextObjHeight = boundary.KLOCSeated[i].height/2;
		var textObjHeight = boundary.KLOCSeated[i].height;
		var itemSpace = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

		//total number of potential / number of current items +1
		if (boundary.KLOCBoundaryName == 'right'){
			boundary.KLOCSeated[i].setAngle(0).setCoords();
			boundary.KLOCSeated[i].setLeft(boundary.aCoords.bl.x+5).setCoords()
		}else if (boundary.KLOCBoundaryName == 'top'){
			boundary.KLOCSeated[i].setAngle(90).setCoords();
			boundary.KLOCSeated[i].setTop(boundary.aCoords.tl.y - boundary.KLOCSeated[i].width-7).setCoords()
		}else if (boundary.KLOCBoundaryName == 'left'){
			boundary.KLOCSeated[i].setAngle(0).setCoords();
			boundary.KLOCSeated[i].setLeft(boundary.aCoords.bl.x - boundary.KLOCSeated[i].width-7).setCoords()
		}else if (boundary.KLOCBoundaryName == 'bottom'){
			boundary.KLOCSeated[i].setAngle(90).setCoords();
			boundary.KLOCSeated[i].setTop(boundary.aCoords.tl.y+5).setCoords()
		}else{
			console.error('fabric_kloc_tables - boudnaryName not set')
		}

		if (boundaryOrientation == 'vertical'){
			boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
			itemSpace = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

			for (j=1; j<textItemsToShuffle.length+1; j++){
				textItemsToShuffle[j-1].setTop(boundary.aCoords.tl.y + ((itemSpace*textObjHeight)*j-halfTextObjHeight));
				textItemsToShuffle[j-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundaryOrientation == 'horizontal'){
			boundary.KLOCMaxBoundarySeating = (parentObj.width*parentObj.scaleX) / textItemsToShuffle[0].height;
			itemSpace = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

			for (j=1; j<textItemsToShuffle.length+1; j++){
				textItemsToShuffle[j-1].setLeft(boundary.aCoords.tl.x + ((itemSpace*textObjHeight)*j+halfTextObjHeight));
				textItemsToShuffle[j-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else{
			console.error('Error - orientation is somehow not set.')
		}

		//set seatingPosition number
		var position = i+1
		var seatingPosition = new fabric.Text(position.toString(), {
			left: 0,
			top: 0,
			fontSize: 12,
			hasControls: false,
			hasRotatingPoint: false,
			lockMovementX: true,
			lockMovementY: true,
			lockScalingFlip: true,
			textAlign: 'center',
			fontFamily: 'Helvetica'
		})
		self.canvasAdd(seatingPosition)
		canvas.bringToFront(seatingPosition)
		boundary.KLOCDinerPosition.push(seatingPosition)
		//position seating markers
		if (boundary.KLOCBoundaryName == 'right'){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2)).setLeft((textItemsToShuffle[i].left-15)-(seatingPosition.width/2))
		}else if (boundary.KLOCBoundaryName == 'top'){
			seatingPosition.setTop(textItemsToShuffle[i].top+(textItemsToShuffle[i].width)+10).setLeft(textItemsToShuffle[i].left-15)
			seatingPosition.setAngle(90).setCoords();
		}else if (boundary.KLOCBoundaryName == 'left'){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2)).setLeft((textItemsToShuffle[i].left+textItemsToShuffle[i].width+15)-(seatingPosition.width/2))
		}else if (boundary.KLOCBoundaryName == 'bottom'){
			seatingPosition.setTop((textItemsToShuffle[i].top-15)-(seatingPosition.height/2)).setLeft((textItemsToShuffle[i].left-(textItemsToShuffle[i].height/2))-(seatingPosition.width/2))
			seatingPosition.setAngle(90).setCoords();
		}else{
			//console.error('fabric_kloc_tables - boudnaryName not set')
		}


	}

}

RectangleTable.prototype.calculateBoundaryCoords = function(groupCoords){
	var self = this;
	console.log('RECTANGLE | calculateBoundaryCoords')
	//we need to reset the aCoords of the object in here - aCoords are canvas relative
	var left = groupCoords.left;
	var top = groupCoords.top;
	var boundaryCount = groupCoords._objects.length;

	for (i=1;i<boundaryCount; i++){

		//new fabric.Point(X,Y)
		switch (i){

			case 1:
				//TOP BOX
				var convertedACoords = {
					bl:new fabric.Point(
						left,
						(top+1)+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					br:new fabric.Point(
						left+(groupCoords._objects[i].width*groupCoords.scaleX),
						(top+1)+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left,
						top+1
					),
					tr:new fabric.Point(
						left+(groupCoords._objects[i].width*groupCoords.scaleX),
						top+1
					)
				}
				break;
			case 2:
				//RIGHT BOX
				var convertedACoords = {
					bl:new fabric.Point(
						left+(groupCoords.width*groupCoords.scaleX)-1,
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					br:new fabric.Point(
						(left+(groupCoords.width*groupCoords.scaleX)-1)+(groupCoords._objects[i].width*groupCoords.scaleX),
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left+(groupCoords.width*groupCoords.scaleX)-1,
						top
					),
					tr:new fabric.Point(
						(left+(groupCoords.width*groupCoords.scaleX)-1)+(groupCoords._objects[i].width*groupCoords.scaleX),
						top
					)
				}
				break;
			case 3:
				// BOTTOM BOX
				var convertedACoords = {
					bl:new fabric.Point(
						left,
						top+(groupCoords.height*groupCoords.scaleY)-1
					),
					br:new fabric.Point(
						left+(groupCoords._objects[i].width*groupCoords.scaleX),
						(top+(groupCoords.height*groupCoords.scaleY)-1) + (groupCoords._objects[i].height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left,
						top+(groupCoords.height*groupCoords.scaleY)-1
					),
					tr:new fabric.Point(
						left+(groupCoords._objects[i].width*groupCoords.scaleX),
						top+(groupCoords.height*groupCoords.scaleY)-1
					)
				}
				break;
			case 4:
				//LEFT BOX
				var convertedACoords = {
					bl:new fabric.Point(
						left+1,
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					br:new fabric.Point(
						left+1+(groupCoords._objects[i].width*groupCoords.scaleX),
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left+1,
						top),
					tr:new fabric.Point(
						left+1+(groupCoords._objects[i].width*groupCoords.scaleX),
						top
					)
				}
				break;

		}

		groupCoords._objects[i].aCoords = convertedACoords;
	}
}

//Add a fabric item to the Canvas
RectangleTable.prototype.canvasAdd = function(name){
	var self = this;
	canvas.add(name);
}

//Add a ghost of the group to the canvas when we move it
RectangleTable.prototype.createGhost = function(group){
	var self = this;
	//console.warn('2spooky4u')
	var ghostOfGroup = new fabric.Rect({
		width: group.cacheWidth,
		height: group.cacheHeight,
		left: group.left,
		top: group.top,
		fill: '#888888'
	});
	self.canvasAdd(ghostOfGroup);
	self.$ghost = ghostOfGroup;
	return ghostOfGroup;
}

//Destroy the ghost when we're done with it
RectangleTable.prototype.destroyGhost = function(ghost){
	var self = this;
	//console.warn('Ghost Busted.');
	canvas.remove(self.$ghost);
}

//Render tables into a group
RectangleTable.prototype.createGroup = function(boundary,trigger,name){
	var self = this;
	var objs = [];

	//For each table created, push it to an array of tables we've made
	self.$boundaries.push({name:name, obj:boundary});

	//Trigger tells us we've rendered the parent, so add all to canvas
	if (trigger == true){

		for (i=0; i < self.$boundaries.length; i++){
			objs.push(self.$boundaries[i].obj)
		}

		//Create Group with Objects defined
		var group = new fabric.Group(objs, {
			left:self.left,
			top:self.top,
			lockScalingFlip: true,
			KLOCGroupOptions : new KLOCGroupOptions({nothing:'insert'}),
			KLOCTableType : 'RectangleTable'
		});


		//Set group as global
		self.$group = group;

		//Add the group to the canvas
		self.canvasAdd(self.$group);

	}
}

//Create a table
RectangleTable.prototype.createTable = function(top,left,width,height,opacity,fill,name,orientation,trigger){
	var self = this;

	//Create rectangle
	var boundary = new fabric.Rect({
		top : top,
		left : left,
		width : width,
		height : height,
		lockScalingFlip: true,
		KLOCSeated : [],
		KLOCTextHeight : 0,
		KLOCMaxBoundarySeating : 0,
		opacity: opacity,
		KLOCBoundaryName : name,
		KLOCOrientation : orientation,
		KLOCDinerPosition : [],
		fill : fill
	});

	//Turn this on to manually add to canvas without grouping
	//self.canvasAdd(name);

	//Feed the created rect into the object creator
	self.createGroup(boundary,trigger,name);

}

//Create the boundary  calculators
RectangleTable.prototype.createParentAndBoundaries = function(){
	var self = this;
	var $side = {width:(self.width/4),height:self.height};
	var $top = {width:self.width,height:(self.height/4)};

	self.createTable(self.top,self.left,self.width,self.height,1,'#cccccc','parent');
	self.createTable(self.top,self.left,self.width,1,1,'','top','horizontal');
	self.createTable(self.top,(self.left+self.width-1),1,self.height,1,'','right','vertical');
	self.createTable((self.top+self.height-1),self.left,self.width,1,1,'','bottom','horizontal');
	self.createTable(self.top,self.left,1,self.height,1,'','left','vertical',true);
}
