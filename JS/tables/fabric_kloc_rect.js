
var KLOCRect = fabric.util.createClass(fabric.Rect, {
    type: 'iRect',
    initialize: function(options) {
        options || (options = { });
		this.kloc ='here';
        this.callSuper('initialize', options);
    },

    fromObject: function (object, callback) {
        return new KLOCRect(object);
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {});
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);

       // custom rendering code goes here ...

       this.canvas.on('mouse:down', function(e) {
           console.log(this.kloc);
       });

    }
});

var iRect = new KLOCRect({
    width: 200,
    height: 100,
    left: 100,
    top: 50,
    fill: '#888'
});

canvas.add(iRect);
