/*
 ██████ ██ ██████   ██████ ██      ███████ ████████  █████  ██████  ██      ███████
██      ██ ██   ██ ██      ██      ██         ██    ██   ██ ██   ██ ██      ██
██      ██ ██████  ██      ██      █████      ██    ███████ ██████  ██      █████
██      ██ ██   ██ ██      ██      ██         ██    ██   ██ ██   ██ ██      ██
 ██████ ██ ██   ██  ██████ ███████ ███████    ██    ██   ██ ██████  ███████ ███████
KLOC 2017
DEV: James Douglas
*/




function CircleTable(dimensions,coords) {
	this.$dimensions = dimensions; //Passed in dimensions
	this.$coords = coords; //Passed in coordinates
	this.size = this.$dimensions.size;
	this.top = this.$coords.top;
	this.left = this.$coords.left;
	this.$boundaries = []; //Array of all created objects, each with name
	this.$group = {}; //The group as an object
	this.$ghost = {};


	this.init();
	this.createParentAndBoundaries();
}

CircleTable.prototype.init = function(){
	var self = this;
}

CircleTable.prototype.reshuffleBoundaryItems = function(boundary,parentObj){
	var self = this;
	console.log('CIRCLETABLE | Reshuffling boundary items');

	var textItemsToShuffle = boundary.KLOCSeated;
	var boundaryOrientation = boundary.KLOCOrientation;

	for (x=0; x<boundary.KLOCDinerPosition.length; x++){
		canvas.remove(boundary.KLOCDinerPosition[x]);
	}

	for (i=0; i<textItemsToShuffle.length; i++){
		var halfTextObjHeight = boundary.KLOCSeated[i].height/2;
		var textObjHeight = boundary.KLOCSeated[i].height;
		var distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);
		boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
		distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

		//Angle is 0 because all text items on circles are horizontal!
		boundary.KLOCSeated[i].setAngle(0).setCoords();

		if (boundary.KLOCBoundaryName == 'right'){
			for (j=1; j<textItemsToShuffle.length+1; j++){
				var distanceFromTop = (distributionOfItems*textObjHeight)*j;
				var radius = (parentObj.height*parentObj.scaleY) / 2;
				var distFromRight = radius - Math.sqrt(Math.abs(Math.pow(radius, 2) - Math.pow(radius - distanceFromTop, 2)));
				textItemsToShuffle[j-1].setLeft((boundary.aCoords.tl.x-distFromRight + 10));
				textItemsToShuffle[j-1].setTop(boundary.aCoords.bl.y + (((distributionOfItems*textObjHeight)*j)-(halfTextObjHeight+6)));
				textItemsToShuffle[j-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'left'){
			for (j=1; j<textItemsToShuffle.length+1; j++){
				var distanceFromTop = (distributionOfItems*textObjHeight)*j;
				var radius = (parentObj.height*parentObj.scaleY) / 2;
				var distFromLeft = radius + Math.sqrt(Math.abs(Math.pow(radius, 2) - Math.pow(radius - distanceFromTop, 2)));
				var parentRight = parentObj.left+(parentObj.width*parentObj.scaleX);
				textItemsToShuffle[j-1].setLeft((parentRight-distFromLeft)-(textItemsToShuffle[j-1].width+10))
				textItemsToShuffle[j-1].setTop(boundary.aCoords.bl.y + (((distributionOfItems*textObjHeight)*j)-(halfTextObjHeight+6)));
				textItemsToShuffle[j-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'top'){
			boundary.KLOCMaxBoundarySeating = 1;
			boundary.KLOCSeated[i].setTop((boundary.aCoords.tl.y - (boundary.KLOCSeated[i].height+5)));
			boundary.KLOCSeated[i].setLeft((parentObj.left+((parentObj.width*parentObj.scaleX)/2)) - (boundary.KLOCSeated[i].width/2))
			boundary.KLOCSeated[i].setCoords();
		}else if (boundary.KLOCBoundaryName == 'bottom'){
			boundary.KLOCMaxBoundarySeating = 1;
			boundary.KLOCSeated[i].setTop(boundary.aCoords.tl.y + 20);
			boundary.KLOCSeated[i].setLeft((parentObj.left+((parentObj.width*parentObj.scaleX)/2)) - (boundary.KLOCSeated[i].width/2))
			boundary.KLOCSeated[i].setCoords();
		}else{
			console.error('fabric_kloc_tables - boudnaryName not set')
		}



		//set seatingPosition number
		var position = i+1
		var seatingPosition = new fabric.Text(position.toString(), {
			left: 0,
			top: 0,
			fontSize: 12,
			hasControls: false,
			hasRotatingPoint: false,
			lockMovementX: true,
			lockMovementY: true,
			lockScalingFlip: true,
			textAlign: 'center',
			fontFamily: 'Helvetica'
		})
		self.canvasAdd(seatingPosition)
		canvas.bringToFront(seatingPosition)
		boundary.KLOCDinerPosition.push(seatingPosition)
		//position seating markers
		if (boundary.KLOCBoundaryName == 'right'){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2))
			seatingPosition.setLeft((textItemsToShuffle[i].left-30)-(seatingPosition.width/2))
		}else if (boundary.KLOCBoundaryName == 'top'){
			seatingPosition.setTop(textItemsToShuffle[i].top+textItemsToShuffle[i].height+20)
			seatingPosition.setLeft((textItemsToShuffle[i].left+(textItemsToShuffle[i].width/2))-(seatingPosition.width/2))
		}else if (boundary.KLOCBoundaryName == 'left'){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2))
			seatingPosition.setLeft((textItemsToShuffle[i].left+textItemsToShuffle[i].width+30)-(seatingPosition.width/2))
		}else if (boundary.KLOCBoundaryName == 'bottom'){
			seatingPosition.setTop((textItemsToShuffle[i].top-30)-(seatingPosition.height/2))
			seatingPosition.setLeft((textItemsToShuffle[i].left+(textItemsToShuffle[i].width/2))-(seatingPosition.width/2))
		}else{
			//console.error('fabric_kloc_tables - boudnaryName not set')
		}


	}
}

CircleTable.prototype.calculateBoundaryCoords = function(groupCoords){
	var self = this;

	console.log('CIRCLETABLE | calcualteboundarycoords')
	//we need to reset the aCoords of the object in here - aCoords are canvas relative
	var left = groupCoords.left;
	var top = groupCoords.top;
	var boundaryCount = groupCoords._objects.length;

	//console.log(boundaryCount)

	for (i=1;i<boundaryCount; i++){
		//break;
		//new fabric.Point(X,Y)
		switch (i){

			case 1:
				//LEFT

				var convertedACoords = {
					bl:new fabric.Point(
						left,
						top+9
					),
					br:new fabric.Point(
						left,
						(top+9)+(groupCoords._objects[i].width*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						(left+61)+(groupCoords._objects[i].height*groupCoords.scaleX),
						top+9
					),
					tr:new fabric.Point(
						(left+61)+(groupCoords._objects[i].height*groupCoords.scaleX),
						(top+9)+(groupCoords._objects[i].width*groupCoords.scaleY)
					)
				}
				break;
			case 2:
				//RIGHT
				var convertedACoords = {
					bl:new fabric.Point(
						left+((groupCoords.width-groupCoords._objects[i].height)*groupCoords.scaleX),
						top+9
					),
					br:new fabric.Point(
						left+((groupCoords.width-groupCoords._objects[i].height)*groupCoords.scaleX),
						(top+9)+(groupCoords._objects[i].width*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left+(groupCoords.width*groupCoords.scaleX),
						top+9
					),
					tr:new fabric.Point(
						left+(groupCoords.width*groupCoords.scaleX),
						(top+9)+(groupCoords._objects[i].width*groupCoords.scaleY)
					)
				}
				break;
			case 3:
				//BOTTOM
				var convertedACoords = {
					bl:new fabric.Point(
						left+(((groupCoords.width/2)-(groupCoords._objects[i].width/2))*groupCoords.scaleX),
						top+(groupCoords.height*groupCoords.scaleY)
					),
					br:new fabric.Point(
						left+((((groupCoords.width/2)-(groupCoords._objects[i].width/2))+groupCoords._objects[i].width)*groupCoords.scaleX),
						top+(groupCoords.height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left+(((groupCoords.width/2)-(groupCoords._objects[i].width/2))*groupCoords.scaleX),
						top+((groupCoords.height-groupCoords._objects[i].height)*groupCoords.scaleY)
					),
					tr:new fabric.Point(
						left+((((groupCoords.width/2)-(groupCoords._objects[i].width/2))+groupCoords._objects[i].width)*groupCoords.scaleX),
						top+((groupCoords.height-groupCoords._objects[i].height)*groupCoords.scaleY)
					)
				}
				break;
			case 4:
				//TOP
				var convertedACoords = {
					bl:new fabric.Point(
						left+(((groupCoords.width/2)-(groupCoords._objects[i].width/2))*groupCoords.scaleX),
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					br:new fabric.Point(
						left+((((groupCoords.width/2)-(groupCoords._objects[i].width/2))+groupCoords._objects[i].width)*groupCoords.scaleX),
						top+(groupCoords._objects[i].height*groupCoords.scaleY)
					),
					tl:new fabric.Point(
						left+(((groupCoords.width/2)-(groupCoords._objects[i].width/2))*groupCoords.scaleX),
						top
					),
					tr:new fabric.Point(
						left+((((groupCoords.width/2)-(groupCoords._objects[i].width/2))+groupCoords._objects[i].width)*groupCoords.scaleX),
						top
					)
				}
				break;
		}

		groupCoords._objects[i].aCoords = convertedACoords;
	}
}

//Add a fabric item to the Canvas
CircleTable.prototype.canvasAdd = function(name){
	var self = this;
	canvas.add(name);
}

//Add a ghost of the group to the canvas when we move it
CircleTable.prototype.createGhost = function(group){
	var self = this;
	//console.warn('2spooky4u')
	var ghostOfGroup = new fabric.Circle({
		radius: (self.size/2)*self.$group.scaleX,
		left: group.left,
		top: group.top,
		fill: '#888888'
	});
	self.canvasAdd(ghostOfGroup);
	self.$ghost = ghostOfGroup;
	return ghostOfGroup;
}

//Destroy the ghost when we're done with it
CircleTable.prototype.destroyGhost = function(ghost){
	var self = this;
	//console.warn('Ghost Busted.');
	canvas.remove(self.$ghost);
}

//Render tables into a group
CircleTable.prototype.createGroup = function(boundary,name,trigger){
	var self = this;
	var objs = [];

	//For each table created, push it to an array of tables we've made
	self.$boundaries.push({name:name, obj:boundary});

	//Trigger tells us we've rendered the parent, so add all to canvas
	if (trigger == true){

		for (i=0; i < self.$boundaries.length; i++){
			objs.push(self.$boundaries[i].obj)
		}

		//Create Group with Objects defined
		var group = new fabric.Group(objs, {
			left:self.left,
			top:self.top,
			lockScalingFlip: true,
			lockUniScaling: true,
			KLOCGroupOptions : new KLOCGroupOptions({nothing:'insert'}),
			KLOCTableType : 'CircleTable'
		});


		//Set group as global
		self.$group = group;

		//Add the group to the canvas
		self.canvasAdd(self.$group);

	}
}

//Create a table
CircleTable.prototype.createTable = function(top,left,size,opacity,fill,name,orientation){
	var self = this;

	//Create rectangle
	var table = new fabric.Circle({
		top: top,
		left: left,
		radius: size/2,
		fill: fill,
		opacity:1,
		KLOCSeated : [],
		KLOCTextHeight : 0,
		KLOCMaxBoundarySeating : 0,
		KLOCBoundaryName : name,
		KLOCOrientation : orientation,
		KLOCDinerPosition : []
	})

	//Turn this on to manually add to canvas without grouping
	//self.canvasAdd(table);

	// //Feed the created rect into the object creator
	self.createGroup(table,name);

}

CircleTable.prototype.createPath = function(top,left,colour,name,arc,angle,trigger){
	var self = this;
	//Dead future me - pls forgive me for this str...
	var arcStr = 'M'+' '+arc.mx+' '+arc.my+' '+'C'+' '+arc.cp1x+' '+arc.cp1y+' '+arc.cp2x+' '+arc.cp2y+' '+arc.epx+' '+arc.epy;
	var line = new fabric.Path(
		//M == START X Y
		//C == CP1X CP1Y CP2X CP2Y ENDX ENDY
		arcStr.toString(),
		{
			top:top,
			left:left,
			angle: angle,
			fill: '',
			stroke: colour,
			objectCaching: false,
			KLOCSeated : [],
			KLOCTextHeight : 0,
			KLOCMaxBoundarySeating : 0,
			KLOCBoundaryName : name,
			KLOCDinerPosition : []
		}
	)
	self.createGroup(line,name,trigger);
}

//Create the boundary  calculators
CircleTable.prototype.createParentAndBoundaries = function(){
	var self = this;
	var $side = {width:(self.width/4),height:self.height};
	var $top = {width:self.width,height:(self.height/4)};

	self.createTable(self.top,self.left,self.size,1,'#cccccc','parent');
	//LEFT
	self.createPath(self.top+9,self.left+61,'','left',{mx:0,my:0,cp1x:32,cp1y:80,cp2x:150,cp2y:80,epx:182,epy:0},90);
	//RIGHT
	self.createPath(self.top+9,self.left+(self.size+1),'','right',{mx:0,my:0,cp1x:32,cp1y:-80,cp2x:150,cp2y:-80,epx:182,epy:0},90);
	//BOTTOM
	self.createPath(self.top+191,self.left+60,'','bottom',{mx:0,my:0,cp1x:25,cp1y:11,cp2x:55,cp2y:11,epx:80,epy:0},0);
	//TOP
	self.createPath(self.top+1,self.left+60,'','top',{mx:0,my:0,cp1x:25,cp1y:-11,cp2x:55,cp2y:-11,epx:80,epy:0},0,true);
};
