/*
	██   ██  ██████  ██████  ███████ ███████ ███████ ██   ██  ██████  ███████
	██   ██ ██    ██ ██   ██ ██      ██      ██      ██   ██ ██    ██ ██
	███████ ██    ██ ██████  ███████ █████   ███████ ███████ ██    ██ █████
	██   ██ ██    ██ ██   ██      ██ ██           ██ ██   ██ ██    ██ ██
	██   ██  ██████  ██   ██ ███████ ███████ ███████ ██   ██  ██████  ███████
	KLOC 2017
	DEV: James Douglas
*/


function HorseshoeTable(dimensions,coords) {
	this.$dimensions = dimensions; //Passed in dimensions
	this.$coords = coords; //Passed in coordinates
	this.size = this.$dimensions.size;
	this.top = this.$coords.top;
	this.left = this.$coords.left;
	this.$boundaries = []; //Array of all created objects, each with name
	this.$group = {}; //The group as an object
	this.$ghost = {};
	this.$KLOCComplexObject = [];
	this.sprigWidth = 60;
	this.sprigHeight = 200;
	this.tempGroup = [];
	this.KLOCFontPresets = [];
	this.dragGhost = {};


	this.init();
	this.createParentAndBoundaries();
}

HorseshoeTable.prototype.init = function(){
	var self = this;
}

HorseshoeTable.prototype.reshuffleBoundaryItems = function(boundary,parentObj){
	var self = this;
	console.info('HorseshoeTable | Reshuffling boundary items');
	// console.log('BOUNDARY',boundary)
	//console.warn('PARENTOBJ',parentObj)

	var textItemsToShuffle = boundary.KLOCSeated;
	var boundaryOrientation = boundary.KLOCOrientation;

	for (x=0; x<boundary.KLOCDinerPosition.length; x++){
		canvas.remove(boundary.KLOCDinerPosition[x]);
	}


	for (i=0; i<textItemsToShuffle.length; i++){
		var halfTextObjHeight = boundary.KLOCSeated[i].height/2;
		var textObjHeight = boundary.KLOCSeated[i].height;
		var distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

		//Angle is 0 because all text items on circles are horizontal!
		boundary.KLOCSeated[i].setAngle(0).setCoords();

		if (boundary.KLOCBoundaryName == 'RS-BR'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

				textItemsToShuffle[textitem-1].setLeft(boundary.aCoords.tl.x+10);
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(halfTextObjHeight+6)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'RS-BL'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

				textItemsToShuffle[textitem-1].setLeft((boundary.aCoords.tl.x-10)-textItemsToShuffle[textitem-1].width);
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(halfTextObjHeight+6)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'RS-BB'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (parentObj.width*parentObj.scaleX) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

				boundary.KLOCSeated[i].setAngle(90).setCoords();
				boundary.KLOCSeated[i].setTop(boundary.aCoords.tl.y+10).setCoords();
				textItemsToShuffle[textitem-1].setLeft(boundary.aCoords.tl.x + (((distributionOfItems*textObjHeight)*textitem)+halfTextObjHeight));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'LS-BL'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				//console.log(boundary.aCoords.tl.x)
				boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);
				textItemsToShuffle[textitem-1].setLeft((boundary.aCoords.tl.x-10)-textItemsToShuffle[textitem-1].width);
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(halfTextObjHeight+6)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'LS-BR'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

				textItemsToShuffle[textitem-1].setLeft(boundary.aCoords.tl.x+10);
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(halfTextObjHeight+6)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'LS-BB'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (parentObj.width*parentObj.scaleX) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);

				boundary.KLOCSeated[i].setAngle(90).setCoords();
				boundary.KLOCSeated[i].setTop(boundary.aCoords.tl.y+10).setCoords();
				textItemsToShuffle[textitem-1].setLeft(boundary.aCoords.tl.x + (((distributionOfItems*textObjHeight)*textitem)+halfTextObjHeight));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'HS-BR'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = ((parentObj.height*parentObj.scaleY)+textObjHeight) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);
				var distanceFromTop = (distributionOfItems*textObjHeight)*textitem;
				var radius = (parentObj.height*parentObj.scaleY)+5;
				var distFromRight = radius-Math.sqrt(Math.pow(radius,2)-Math.pow(radius-distanceFromTop, 2));
				textItemsToShuffle[textitem-1].setLeft((parentObj.aCoords.tr.x-distFromRight + 10));
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(textObjHeight)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else if (boundary.KLOCBoundaryName == 'HS-BL'){
			for (textitem=1; textitem<textItemsToShuffle.length+1; textitem++){
				boundary.KLOCMaxBoundarySeating = (((parentObj.height)*parentObj.scaleY)+textObjHeight) / textItemsToShuffle[0].height;
				distributionOfItems = boundary.KLOCMaxBoundarySeating / (boundary.KLOCSeated.length+1);
				distanceFromTop = (distributionOfItems*textObjHeight)*textitem;
				radius = (parentObj.height*parentObj.scaleY)+5;
				var distFromRight = Math.sqrt(Math.pow(radius,2)-Math.pow(radius-distanceFromTop, 2));
				textItemsToShuffle[textitem-1].setLeft(((parentObj.aCoords.tl.x+((parentObj.width*parentObj.scaleX)/2))-distFromRight + 10)-(textItemsToShuffle[textitem-1].width+10));
				textItemsToShuffle[textitem-1].setTop(boundary.aCoords.tl.y + (((distributionOfItems*textObjHeight)*textitem)-(textObjHeight)));
				textItemsToShuffle[textitem-1].setCoords(); //Update text coords, since they've been manually reset
			}
		}else{
			console.error('HORSESHOE | fabric_kloc_tables - boudnaryName not set');
		}


		//set seatingPosition number
		var position = i+1;
		var seatingPosition = new fabric.Text(position.toString(), {
			left: 0,
			top: 0,
			fontSize: 12,
			hasControls: false,
			hasRotatingPoint: false,
			lockMovementX: true,
			lockMovementY: true,
			lockScalingFlip: true,
			textAlign: 'center',
			fontFamily: 'Helvetica'
		});
		self.canvasAdd(seatingPosition);
		canvas.bringToFront(seatingPosition);
		boundary.KLOCDinerPosition.push(seatingPosition);
		//position seating markers
		if (boundary.KLOCBoundaryName == 'RS-BR' || boundary.KLOCBoundaryName == 'HS-BR' || boundary.KLOCBoundaryName == 'LS-BR'){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2));
			seatingPosition.setLeft((textItemsToShuffle[i].left-30)-(seatingPosition.width/2));
		}else if (boundary.KLOCBoundaryName == 'RS-BB' || boundary.KLOCBoundaryName == 'LS-BB'){
			seatingPosition.setTop((textItemsToShuffle[i].top-30)-(seatingPosition.height/2));
			seatingPosition.setLeft((textItemsToShuffle[i].left-(textItemsToShuffle[i].height/2))-(seatingPosition.width/2));
			seatingPosition.setAngle(90).setCoords();
		}else if (boundary.KLOCBoundaryName == 'LS-BL' || boundary.KLOCBoundaryName == 'HS-BL' || boundary.KLOCBoundaryName == 'RS-BL' ){
			seatingPosition.setTop((textItemsToShuffle[i].top+(textItemsToShuffle[i].height/2))-(seatingPosition.height/2));
			seatingPosition.setLeft((textItemsToShuffle[i].left+textItemsToShuffle[i].width+30)-(seatingPosition.width/2));
		}else{

		}

	}
};

HorseshoeTable.prototype.calculateGhostMovement = function(passedInObj){
	var self = this;
	console.log('HorseshoeTable | calculateGhostMovement')
	if (passedInObj != undefined){
		switch (passedInObj.KLOCTableType){

			//Horseshoe Boundary Left
			case "sprig-left":
				self.dragGhost.setLeft(passedInObj.getLeft())
				self.dragGhost.setTop(passedInObj.getTop()-(passedInObj.KLOCComplexObjectGroup[0].height*passedInObj.KLOCComplexObjectGroup[0].scaleY))
				break;
			case "sprig-right":
				self.dragGhost.setLeft(passedInObj.getLeft()-(passedInObj.KLOCComplexObjectGroup[0].width*passedInObj.KLOCComplexObjectGroup[0].scaleX)+(passedInObj.width*passedInObj.scaleX))
				self.dragGhost.setTop(passedInObj.getTop()-(passedInObj.KLOCComplexObjectGroup[0].height*passedInObj.KLOCComplexObjectGroup[0].scaleY))
				break;
			case "bezier":
				self.dragGhost.setLeft(passedInObj.getLeft())
				self.dragGhost.setTop(passedInObj.getTop())
				break;
			default:
		}
	}
}

HorseshoeTable.prototype.calculateBoundaryCoords = function(passedInObj){
	var self = this;
	console.log('HorseshoeTable | calcualteboundarycoords');

	//we need to reset the aCoords of the object in here - aCoords are canvas relative
	//find total of fake grouped objects
	//dive through them to find the boundaries and reset all their coords. Fun.
	for (x=0; x<self.$KLOCComplexObject.length; x++){
		for (y=1; y<self.$KLOCComplexObject[x]._objects.length; y++){
			// console.log(self.$KLOCComplexObject[x]._objects[y].KLOCBoundaryName,self.$KLOCComplexObject[x]._objects[y])

			switch (self.$KLOCComplexObject[x]._objects[y].KLOCBoundaryName){

				//Horseshoe Boundary Left
				case "HS-BL":
					var convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x]._objects[y].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x]._objects[y].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				//Horseshoe Boundary Right
				case "HS-BR":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left+((self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX)/2),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							(self.$KLOCComplexObject[x].left+((self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX)/2))+(self.$KLOCComplexObject[x]._objects[y].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left+((self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX)/2),
							self.$KLOCComplexObject[x].top
						),
						tr:new fabric.Point(
							(self.$KLOCComplexObject[x].left+((self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX)/2))+(self.$KLOCComplexObject[x]._objects[y].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				//Right Sprig boundary right
				case "RS-BR":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
					//Right Sprig boundary right
					case "RS-BL":
						convertedACoords = {
							bl:new fabric.Point(
								self.$KLOCComplexObject[x].left,
								self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
							),
							br:new fabric.Point(
								self.$KLOCComplexObject[x].left,
								self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
							),
							tl:new fabric.Point(
								self.$KLOCComplexObject[x].left,
								self.$KLOCComplexObject[x].top
							),
							tr:new fabric.Point(
								self.$KLOCComplexObject[x].left,
								self.$KLOCComplexObject[x].top
							)
						};
						self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
						break;
				//Right Sprig boundary bottom
				case "RS-BB":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)-(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x]._objects[y].scaleY)
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)-(self.$KLOCComplexObject[x]._objects[y].height*self.$KLOCComplexObject[x]._objects[y].scaleY)
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				//Left Sprig boundary Left
				case "LS-BL":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				//Left Sprig boundary Left
				case "LS-BR":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				//Left Sprig boundary Left
				case "LS-BB":
					convertedACoords = {
						bl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						br:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tl:new fabric.Point(
							self.$KLOCComplexObject[x].left,
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						),
						tr:new fabric.Point(
							self.$KLOCComplexObject[x].left+(self.$KLOCComplexObject[x].width*self.$KLOCComplexObject[x].scaleX),
							self.$KLOCComplexObject[x].top+(self.$KLOCComplexObject[x].height*self.$KLOCComplexObject[x].scaleY)
						)
					};
					self.$KLOCComplexObject[x]._objects[y].aCoords = convertedACoords;
					break;
				default:
					console.error('Horseshoe boundary aCoords calc error ohno');
					break;
			}
		}
	}

};
//Add a fabric item to the Canvas
HorseshoeTable.prototype.canvasAdd = function(name){
	var self = this;
	canvas.add(name);
}
//Add a ghost of the group to the canvas when we move it
HorseshoeTable.prototype.createGhost = function(passedInObj){
	var self = this;
	//console.warn('THIS HORSE IS 2spooky4u')
	//console.log(passedInObj)

	var bezier = self.createHorseshoe(passedInObj.KLOCComplexObjectGroup[0].getTop(),passedInObj.KLOCComplexObjectGroup[0].getLeft(),0,1,'#cccccc','parent')
	bezier.setScaleX(passedInObj.KLOCComplexObjectGroup[0].getScaleX())
	bezier.setScaleY(passedInObj.KLOCComplexObjectGroup[0].getScaleY())
	var sprigR = self.createTable(passedInObj.KLOCComplexObjectGroup[1].getTop(),passedInObj.KLOCComplexObjectGroup[1].getLeft(),passedInObj.KLOCComplexObjectGroup[1].getWidth(),passedInObj.KLOCComplexObjectGroup[1].getHeight(),1,'#cccccc','sprig-right')
	var sprigL = self.createTable(passedInObj.KLOCComplexObjectGroup[2].getTop(),passedInObj.KLOCComplexObjectGroup[2].getLeft(),passedInObj.KLOCComplexObjectGroup[2].getWidth(),passedInObj.KLOCComplexObjectGroup[2].getHeight(),1,'#cccccc','sprig-left')

	self.$ghost = [bezier,sprigR,sprigL]
	if (passedInObj.KLOCTableType == "sprig-right"){
		self.canvasAdd(sprigR)
	}else if (passedInObj.KLOCTableType == "sprig-left"){
		self.canvasAdd(sprigL)
	}else if (passedInObj.KLOCTableType == "bezier"){
		self.canvasAdd(bezier)
	}else{

	}

	self.createDragGhost(passedInObj);
}

HorseshoeTable.prototype.createDragGhost = function(passedInObj){
	var self = this;

	var dragGhost = []
	//console.log(passedInObj)
	var bezier = self.createHorseshoe(self.top,self.left,self.size,1,'#cccccc','GHOSTBEZ')
	bezier.setScaleX(passedInObj.KLOCComplexObjectGroup[0].getScaleX())
	bezier.setScaleY(passedInObj.KLOCComplexObjectGroup[0].getScaleY())
	var sprigR = self.createTable(self.top+(bezier.height*bezier.scaleY),self.left+((bezier.width*bezier.scaleX)-(self.sprigWidth*passedInObj.KLOCComplexObjectGroup[1].scaleX)),(self.sprigWidth*bezier.scaleX),self.sprigHeight*passedInObj.KLOCComplexObjectGroup[1].scaleY,1,'#cccccc','GHOSTR')
	var sprigL = self.createTable(self.top+(bezier.height*bezier.scaleY),self.left,(self.sprigWidth*passedInObj.KLOCComplexObjectGroup[2].scaleX),(self.sprigHeight*passedInObj.KLOCComplexObjectGroup[2].scaleY),1,'#cccccc','GHOSTL')

	dragGhost.push(bezier,sprigR,sprigL)

	var ghostGroup = new fabric.Group(dragGhost, {
		left:passedInObj.KLOCComplexObjectGroup[0].getLeft(),
		top:passedInObj.KLOCComplexObjectGroup[0].getTop(),
		lockScalingFlip: true,
		lockRotation: true,
		KLOCGhostObject: true
	});

	self.dragGhost = ghostGroup
	self.canvasAdd(ghostGroup)
};
//Destroy the ghost when we're done with it
HorseshoeTable.prototype.destroyGhost = function(ghost){
	var self = this;
	//console.warn('Ghost Busted.');
	for (i=0; i<self.$ghost.length;i++){
		canvas.remove(self.$ghost[i]);
	}
	canvas.remove(self.dragGhost);

}
//Render tables into a group
HorseshoeTable.prototype.renderKLOCComplexGroup = function(){
	var self = this;

	for (j=0; j<self.$KLOCComplexObject.length;j++){
		self.canvasAdd(self.$KLOCComplexObject[j]);
		self.$KLOCComplexObject[j].setCoords()
	}
	//calc all boundary coords, because apparently they're not pre-calculated..
	self.calculateBoundaryCoords()


}

HorseshoeTable.prototype.createGroup = function(groupedObjs,coords,name){
	var self = this;

	var group = new fabric.Group(groupedObjs, {
		left:coords.left,
		top:coords.top,
		lockScalingFlip: true,
		lockRotation: true,
		KLOCTableType : name,
		KLOCComplexObjectGroup: self.$KLOCComplexObject,
		KLOCComplexParent: self
	});
	self.$KLOCComplexObject.push(group)
}
//reset positions of fake-group items when any of the items are moved / resized.
HorseshoeTable.prototype.setKLOCComplexObjectPosition = function(obj){
	var self = this;
	console.log('HorseshoeTable | setKLOCComplexObjectPosition')

	//If horseshoe selected
	switch (obj.KLOCTableType){
		case 'bezier':
			for (i=0;i<self.$KLOCComplexObject.length; i++){
				switch(self.$KLOCComplexObject[i].KLOCTableType){
					case obj.KLOCTableType:

						break;
					case "sprig-left":
						self.$KLOCComplexObject[i].setTop((obj.height*obj.scaleY)+obj.top-4);
						self.$KLOCComplexObject[i].setScaleX(obj.scaleX);
						self.$KLOCComplexObject[i].setLeft(obj.left);
						break;
					case "sprig-right":
						self.$KLOCComplexObject[i].setTop((obj.height*obj.scaleY)+obj.top-4);
						self.$KLOCComplexObject[i].setScaleX(obj.scaleX);
						self.$KLOCComplexObject[i].setLeft((obj.left+obj.width*obj.scaleX)-(self.$KLOCComplexObject[i].width*self.$KLOCComplexObject[i].getScaleX()));
						break;
					default :
						console.error('Error case: Bezier')
						break;
				}
				self.$KLOCComplexObject[i].setCoords();
			}
			break;
		case 'sprig-left':
			for (i=0;i<self.$KLOCComplexObject.length; i++){
				switch(self.$KLOCComplexObject[i].KLOCTableType){
					case obj.KLOCTableType:

						break;
					case "bezier":
						self.$KLOCComplexObject[i].setTop(obj.top-(self.$KLOCComplexObject[i].height*self.$KLOCComplexObject[i].scaleY)+4);
						self.$KLOCComplexObject[i].setLeft(obj.left);
						break;
					case "sprig-right":
						self.$KLOCComplexObject[i].setTop(obj.top);
						self.$KLOCComplexObject[i].setLeft((obj.left+(self.$KLOCComplexObject[0].width*self.$KLOCComplexObject[0].scaleX))-(obj.width*obj.scaleX));
						break;
					default :
						console.error('Error case: Sprig-left')
						break;
				}
				self.$KLOCComplexObject[i].setCoords();
			}
			break;
		case 'sprig-right':
			for (i=0;i<self.$KLOCComplexObject.length; i++){
				switch(self.$KLOCComplexObject[i].KLOCTableType){
					case obj.KLOCTableType:

						break;
					case "bezier":
						self.$KLOCComplexObject[i].setTop(obj.top-(self.$KLOCComplexObject[i].height*self.$KLOCComplexObject[i].scaleY)+4);
						self.$KLOCComplexObject[i].setLeft((obj.left-(self.$KLOCComplexObject[i].width*self.$KLOCComplexObject[i].scaleX))+(obj.width*obj.scaleX));
						break;
					case "sprig-left":
						self.$KLOCComplexObject[i].setTop(obj.top);
						self.$KLOCComplexObject[i].setLeft((obj.left+(obj.width*obj.scaleX))-(self.$KLOCComplexObject[0].width*self.$KLOCComplexObject[0].scaleX));
						break;
					default :
						console.error('Error case: Sprig-left')
						break;
				}
				self.$KLOCComplexObject[i].setCoords();
			}
			break;
		default:
			console.log('This should not be possible')
			break;
	}

}
//Create a table
HorseshoeTable.prototype.createHorseshoe = function(top,left,size,opacity,fill,name){
	var self = this;

	var mylovelyhorse = new fabric.Path(
		//http://fabricjs.com/docs/fabric.js.html#line16764
		//M == START X Y
		//C == CP1X CP1Y CP2X CP2Y ENDX ENDY - BEZIER CURVE
		//h == HORIZONTAL POINT (move horizontally)

		// x y		  CP1X   CP1Y   CP2X  CP2Y  ENDX  ENDY	|	HX	 HY	|  CP1X   CP1Y   CP2X  CP2Y  ENDX  ENDY
		//'M 0 0 		C 40,    -200,  360,  -200, 400,  0		h   -120, 0  C 100,   -20,     200,  -50,    120,  0',

		'M 0 0 		C 0,-'+(self.size+60)+','+(self.size*2)+',-'+(self.size+60)+','+(self.size*2)+',0    h -60   C '+(self.size+95)+',-'+(self.size-20)+', 60,-'+(self.size-20)+',60,0     h -60   ',
		{
			top:top,
			left:left,
			angle: 0,
			fill: fill,
			stroke: '',
			objectCaching: false,
			lockUniScaling: true,
			lockRotation: true,
			KLOCSeated : [],
			KLOCTextHeight : 0,
			KLOCMaxBoundarySeating : 0,
			KLOCBoundaryName : name,
			KLOCDinerPosition : []
		}
	);

	return mylovelyhorse;

};
//Create a curved boundary (using paths)
HorseshoeTable.prototype.createBezier = function(top,left,size,opacity,fill,name,pathmap){
	var self = this;

	var bezier = new fabric.Path(
		pathmap,
		{
			top:top,
			left:left,
			angle: 0,
			fill: '',
			stroke: fill,
			objectCaching: false,
			lockUniScaling: true,
			lockRotation: true,
			hasControls: false,
			KLOCSeated : [],
			KLOCTextHeight : 0,
			KLOCMaxBoundarySeating : 0,
			KLOCBoundaryName : name,
			KLOCObjectType : 'bezier-curve',
			KLOCDinerPosition : []
		}
	)

	self.$boundaries.push(bezier)

	return bezier
}
//Create a table
HorseshoeTable.prototype.createRect = function(top,left,width,height,opacity,fill,name){
	var self = this;

	//Create rectangle
	var getRekt = new fabric.Rect({
		top : top,
		left : left,
		width : width,
		height : height,
		KLOCSeated : [],
		KLOCTextHeight : 0,
		lockScalingX: true,
		hasControls: false,
		KLOCMaxBoundarySeating : 0,
		opacity: opacity,
		KLOCBoundaryName : name,
		KLOCDinerPosition : [],
		fill : fill
	});

	self.$boundaries.push(getRekt)

	return getRekt;

}
//Create a Sprig
HorseshoeTable.prototype.createTable = function(top,left,width,height,opacity,fill,name){
	var self = this;

	//Create rectangle
	var sprig = new fabric.Rect({
		top : top,
		left : left,
		width : width,
		height : height,
		KLOCSeated : [],
		KLOCTextHeight : 0,
		lockScalingX: true,
		KLOCMaxBoundarySeating : 0,
		opacity: opacity,
		KLOCBoundaryName : name,
		KLOCDinerPosition : [],
		fill : fill
	});
	return sprig

}
//Create the boundary  calculators
HorseshoeTable.prototype.createParentAndBoundaries = function(){
	var self = this;

	for (i=0; i<4; i++){
		//top,left,width,height,fill

		//x y		  CP1X   CP1Y   CP2X  CP2Y  ENDX  ENDY
		switch (i){

			case 0:
				self.createGroup(
					[
						self.createHorseshoe(self.top,self.left,self.size,1,'#cccccc','parent'),
						self.createBezier(self.top,self.left,self.size,1,'#cccccc','HS-BL',
						'M 0 '+(self.size+8)+' C 0,11,130,0,'+self.size+',0'),
						self.createBezier(self.top,self.left+self.size,self.size,1,'#cccccc','HS-BR',
						'M 0 0 C 0,-2,150,0,'+self.size+','+(self.size+8))
					],
					{left:self.left,top:self.top},
					"bezier"
				)
				break;
			case 1:
				//SPRIG R
				self.createGroup(
					[
						self.createTable(0,0,self.sprigWidth,self.sprigHeight,1,'#cccccc','sprig-right'),
						self.createRect(0,self.sprigWidth-1,1,self.sprigHeight,1,'#cccccc','RS-BR'),
						self.createRect(self.sprigHeight-1,0,self.sprigWidth,1,1,'#cccccc','RS-BB'),
						self.createRect(0,0,1,self.sprigHeight,1,'#cccccc','RS-BL')
					],
					{left:self.left+240,top:self.top+157},
					"sprig-right"
				)
				break;
			case 2:
				//SPRIG L
				self.createGroup(
					[
						self.createTable(0,0,self.sprigWidth,self.sprigHeight,1,'#cccccc','sprig-left'),
						self.createRect(0,0,1,self.sprigHeight,1,'#cccccc','LS-BL'),
						self.createRect(self.sprigHeight-1,0,self.sprigWidth,1,1,'#cccccc','LS-BB'),
						self.createRect(0,self.sprigWidth-1,1,self.sprigHeight,1,'#cccccc','LS-BR')
					],
					{left:self.left,top:self.top+157},
					"sprig-left"
				)
				break;
			case 3:
				//console.log('case 3')
				//FINAL CASE - RUN THROUGH ALL ITEMS ADDED TO THE ARRAY OF S--T WE'VE CREATED, RENDER THAT TO CANVAS
				self.renderKLOCComplexGroup()
				break;

		}
	}

}
