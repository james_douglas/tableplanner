/*
 ██████  ██       ██████  ██████   █████  ██      ███████  ██████  ███    ██ ████████ ███████
██       ██      ██    ██ ██   ██ ██   ██ ██      ██      ██    ██ ████   ██    ██    ██
██   ███ ██      ██    ██ ██████  ███████ ██      █████   ██    ██ ██ ██  ██    ██    ███████
██    ██ ██      ██    ██ ██   ██ ██   ██ ██      ██      ██    ██ ██  ██ ██    ██         ██
 ██████  ███████  ██████  ██████  ██   ██ ███████ ██       ██████  ██   ████    ██    ███████
KLOC 2017
DEV: James Douglas
*/

function FontsStyle(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change

	this.fontSize = 18;
	this.lineHeight = 18;
	this.opacity = 1;
	this.fontFamily = 'arial';
	this.fontStyle = 'normal';
	this.fontColour = '#000000';
	this.fontBackground = '#ffffff';
	this.strokeWidth = 0;
	this.strokeColour = '#ffffff';

	this.init();
	this.clickBindings();
	this.getGlobalFontProperties();
}
//INITIALISE JQUERY UI SLIDER
FontsStyle.prototype.init = function(){
	var self = this;

	//Set Jquery UI slider defaults
	$( ".slider" ).slider({
		range: "min"
	});
	$( ".slider" ).slider( "value", 18 );
	$( ".slider-onescale" ).slider( "value", 1 );
	$( ".slider-strokecolour" ).slider( "value", 0 );
	$( ".slider-onescale" ).slider({
		max :1,
		step:0.1
	});
	$( ".slider-strokecolour" ).slider({
		max :10,
		step:1
	});
}
/*
███████ ███████ ████████      ██████  ██████  ███    ██ ████████ ███████ ██   ██ ████████
██      ██         ██        ██      ██    ██ ████   ██    ██    ██       ██ ██     ██
███████ █████      ██        ██      ██    ██ ██ ██  ██    ██    █████     ███      ██
     ██ ██         ██        ██      ██    ██ ██  ██ ██    ██    ██       ██ ██     ██
███████ ███████    ██         ██████  ██████  ██   ████    ██    ███████ ██   ██    ██
*/
FontsStyle.prototype.setContextMenu = function(obj){
	var self = this;
	console.log('FONTS | setContextMenu');

	function setCustomFontsOnGroupedObject(object){
		if (object.KLOCFontPresets === undefined){
			object.KLOCFontPresets = {
				fontsize: self.fontSize,
				fontlineheight: self.lineHeight,
				fontopacity: self.opacity,
				fontstrokewidth: self.strokeWidth,
				fontfamily: self.fontFamily,
				fontcolour: self.fontColour,
				fontbackground: self.fontBackground,
				strokecolour: self.strokeColour,
				fontstyle: self.fontStyle
			};
		}
		$('.single-fontsize').val(object.KLOCFontPresets.fontsize);
		$('.single-lineheight').val(object.KLOCFontPresets.fontlineheight );
		$('.single-opacity').val(object.KLOCFontPresets.fontopacity );
		$('.single-strokewidth').val(object.KLOCFontPresets.fontstrokewidth );
		$('.single-fontfamily').val(object.KLOCFontPresets.fontfamily).prop('selected', true);
		$('.single-font-colour').val(object.KLOCFontPresets.fontcolour);
		$('.single-font-backgroundcolour').val(object.KLOCFontPresets.fontbackground);
		$('.single-font-strokecolour').val(object.KLOCFontPresets.fontstroke);
		if (object.KLOCFontPresets.fontweight != 'normal'){
			$('.single-fontstyle').val(object.KLOCFontPresets.fontweight).prop('selected', true);
		}else if (object.KLOCFontPresets.fontstyle != 'normal'){
			$('.single-fontstyle').val(object.KLOCFontPresets.fontstyle).prop('selected', true);
		}else if (object.KLOCFontPresets.fontdecoration != 'normal'){
			if (object.KLOCFontPresets.fontdecoration == 'line-through'){
				$('.single-fontstyle').val('linethrough').prop('selected', true);
			}else{
				$('.single-fontstyle').val(object.KLOCFontPresets.fontdecoration).prop('selected', true);
			}
		}else{
			$('.single-fontstyle').val('normal').prop('selected', true);
		}
	}
	function setCustomFontsOnComplexGroupedObject(object){
		console.log(object);
		if (object.KLOCFontPresets === undefined || object.KLOCFontPresets.length === 0){
			object.KLOCFontPresets = {
				fontsize: self.fontSize,
				fontlineheight: self.lineHeight,
				fontopacity: self.opacity,
				fontstrokewidth: self.strokeWidth,
				fontfamily: self.fontFamily,
				fontcolour: self.fontColour,
				fontbackground: self.fontBackground,
				strokecolour: self.strokeColour,
				fontstyle: self.fontStyle
			};
		}
		$('.single-fontsize').val(object.KLOCFontPresets.fontsize );
		$('.single-lineheight').val(object.KLOCFontPresets.fontlineheight );
		$('.single-opacity').val(object.KLOCFontPresets.fontopacity );
		$('.single-strokewidth').val(object.KLOCFontPresets.fontstrokewidth );
		$('.single-fontfamily').val(object.KLOCFontPresets.fontfamily).prop('selected', true);
		$('.single-font-colour').val(object.KLOCFontPresets.fontcolour);
		$('.single-font-backgroundcolour').val(object.KLOCFontPresets.fontbackground);
		$('.single-font-strokecolour').val(object.KLOCFontPresets.fontstroke);
		if (object.KLOCFontPresets.fontweight != 'normal'){
			$('.single-fontstyle').val(object.KLOCFontPresets.fontweight).prop('selected', true);
		}else if (object.KLOCFontPresets.fontstyle != 'normal'){
			$('.single-fontstyle').val(object.KLOCFontPresets.fontstyle).prop('selected', true);
		}else if (object.KLOCFontPresets.fontdecoration != 'normal'){
			if (object.KLOCFontPresets.fontdecoration == 'line-through'){
				$('.single-fontstyle').val('linethrough').prop('selected', true);
			}else{
				$('.single-fontstyle').val(object.KLOCFontPresets.fontdecoration).prop('selected', true);
			}
		}else{
			$('.single-fontstyle').val('normal').prop('selected', true);
		}
	}
	//console.log('context menu | ',obj)
	//If we clicked on a group with boundaries
	if (obj._objects !== undefined && obj.KLOCComplexObjectGroup === undefined){
		console.log('group with boundaries')
		setCustomFontsOnGroupedObject(obj);
	}else if (obj._objects !== undefined && obj.KLOCComplexObjectGroup !== undefined){
		console.log('Complex group with boundaries')
		setCustomFontsOnComplexGroupedObject(obj.KLOCComplexParent);
	}else if (obj._objects === undefined){
		console.log('Text item?')
	//if we clicked on a text object, just target it to change it's properties
		var fontColour = obj.getFill();
		var fontBackground = obj.getTextBackgroundColor();
		var fontFamily = obj.getFontFamily();
		var fontSize = obj.getFontSize();
		var fontStyle = obj.getFontStyle();
		var fontWeight = obj.getFontWeight();
		var fontLineHeight = obj.getLineHeight();
		var fontOpacity = obj.getOpacity();
		var fontStoke = obj.getStroke();
		var fontStokeWidth = obj.getStrokeWidth();
		var fontDecoration = obj.getTextDecoration();

		$('.single-fontsize').val(fontSize );
		$('.single-lineheight').val(fontLineHeight );
		$('.single-opacity').val(fontOpacity );
		$('.single-strokewidth').val(fontStokeWidth );
		$('.single-fontfamily').val(fontFamily).prop('selected', true);
		$('.single-font-colour').val(fontColour);
		$('.single-font-backgroundcolour').val(fontBackground);
		$('.single-font-strokecolour').val(fontStoke);
		if (fontWeight != 'normal'){
			$('.single-fontstyle').val(fontWeight).prop('selected', true);
		}else if (fontStyle != 'normal'){
			$('.single-fontstyle').val(fontStyle).prop('selected', true);
		}else if (fontDecoration != 'normal'){
			if (fontDecoration == 'line-through'){
				$('.single-fontstyle').val('linethrough').prop('selected', true);
			}else{
				$('.single-fontstyle').val(fontDecoration).prop('selected', true);
			}
		}else{
			$('.single-fontstyle').val('normal').prop('selected', true);
		}
	}else{

	}

};
/*
 ██████ ██      ██  ██████ ██   ██     ██████  ██ ███    ██ ██████  ██ ███    ██  ██████
██      ██      ██ ██      ██  ██      ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██
██      ██      ██ ██      █████       ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███
██      ██      ██ ██      ██  ██      ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██
 ██████ ███████ ██  ██████ ██   ██     ██████  ██ ██   ████ ██████  ██ ██   ████  ██████
*/


FontsStyle.prototype.clickBindings = function(){
	var self = this;

	//GLOBAL
	//on slider change

	$('.update-single-font').click(function(){
		self.setSingleFontProperties($(this).parent());
	});

	$('.update-global-font').click(function(){
		self.setGlobalFontProperties();
	});

	//colour reset
	$('.color-reset').click(function(){
		if ($(this).prev().hasClass("rightclick-font-context") === true){
			$(this).prev().addClass('delete-single-fill');
			if ($(this).prev().attr('data-textattr') == 'fontcolour'){
				$(this).prev().val(self.fontColour);
			}else if($(this).prev().attr('data-textattr') == 'fontbackground'){
				$(this).prev().val(self.fontBackground);
			}else if($(this).prev().attr('data-textattr') == 'strokecolour'){
				$(this).prev().val(self.strokeColour);
			}
			self.setSingleFontProperties($(this).parent());
		}else{
			$(this).prev().addClass('delete-global-fill');
			self.setGlobalFontProperties();
		}
	});

	$(document).on('click','.rightclick-font-context-close',function(){
		//Reset the targetObject for the right click so it can't get accidentally retargetted
		rightClickState.targetObject = {};
		$('#rightclick-font-context').hide();
	});

	$(document).on('click','.reset-single-styling',function(){
		self.reassignGlobalFontProperties(rightClickState.targetObject);
		if (rightClickState.targetObject.KLOCComplexParent !== undefined){
			rightClickState.targetObject.KLOCComplexParent.KLOCFontPresets = [];
			delete rightClickState.targetObject.KLOCComplexParent.KLOCCustomFont;
			//rightClickState.targetObject = {};
		}else{
			delete rightClickState.targetObject.KLOCFontPresets;
			delete rightClickState.targetObject.KLOCCustomFont;
			//rightClickState.targetObject = {};
		}
	});
};
//GET CURRENT PROPERTIES ASSIGNED TO GLOBAL TEXT
FontsStyle.prototype.reassignGlobalFontProperties = function(thisObj){
	var self = this;
	var target = thisObj.KLOCCustomFont;
	console.log('FONTS | reassignGlobalFontProperties');
	//Are we inside a group?
	if (thisObj._objects !== undefined && thisObj.KLOCComplexParent === undefined){
		for (i=1; i<thisObj._objects.length; i++){
			//for each boundary
			for (x=0; x<thisObj._objects[i].KLOCSeated.length; x++){

				//set the flag on each text item inside a boundary to say it's been given a custom font proeprty
				for (a=0; a<canvasText.length; a++){
					if (canvasText[a] == thisObj._objects[i].KLOCSeated[x]){
						canvasText[a].KLOCCustomFont = true;
					}
				}

				thisObj._objects[i].KLOCSeated[x].setFontSize(self.fontSize);
				thisObj._objects[i].KLOCSeated[x].setLineHeight(self.lineHeight);
				thisObj._objects[i].KLOCSeated[x].setOpacity(self.opacity);
				thisObj._objects[i].KLOCSeated[x].fontFamily = self.fontFamily;
				thisObj._objects[i].KLOCSeated[x].setColor(self.fontColour);
				thisObj._objects[i].KLOCSeated[x].setTextBackgroundColor(self.fontBackground);
				if (self.fontStyle == 'bold'){
					thisObj._objects[i].KLOCSeated[x].setFontWeight('bold');
					delete thisObj._objects[i].KLOCSeated[x].fontStyle;
					delete thisObj._objects[i].KLOCSeated[x].textDecoration;
				}else if (self.fontStyle == 'italic'){
					delete thisObj._objects[i].KLOCSeated[x].fontWeight;
					thisObj._objects[i].KLOCSeated[x].setFontStyle('italic');
					delete thisObj._objects[i].KLOCSeated[x].textDecoration;
				}else if (self.fontStyle == 'normal'){
					delete thisObj._objects[i].KLOCSeated[x].fontWeight;
					delete thisObj._objects[i].KLOCSeated[x].fontStyle;
					delete thisObj._objects[i].KLOCSeated[x].textDecoration;
				}else if (self.fontStyle == 'linethrough'){
					delete thisObj._objects[i].KLOCSeated[x].fontWeight;
					delete thisObj._objects[i].KLOCSeated[x].fontStyle;
					thisObj._objects[i].KLOCSeated[x].setTextDecoration('line-through');
				}else if (self.fontStyle == 'underline'){
					delete thisObj._objects[i].KLOCSeated[x].fontWeight;
					delete thisObj._objects[i].KLOCSeated[x].fontStyle;
					thisObj._objects[i].KLOCSeated[x].setTextDecoration('underline');
				}else if (self.fontStyle == 'overline'){
					delete thisObj._objects[i].KLOCSeated[x].fontWeight;
					delete thisObj._objects[i].KLOCSeated[x].fontStyle;
					thisObj._objects[i].KLOCSeated[x].setTextDecoration('overline');
				}
				if (self.stroke == "0" || self.stroke === 0){
					//thisObj._objects[i].KLOCSeated[x].setStroke("0");
					delete thisObj._objects[i].KLOCSeated[x].strokeWidth;
					delete thisObj._objects[i].KLOCSeated[x].stroke;
				}else{
					thisObj._objects[i].KLOCSeated[x].setStrokeWidth(self.strokeWidth);
				}

				if (thisObj._objects[i].KLOCSeated[x].strokeColour === '' || thisObj._objects[i].KLOCSeated[x].strokeColour === undefined){
					delete thisObj._objects[i].KLOCSeated[x].strokeWidth;
					delete thisObj._objects[i].KLOCSeated[x].stroke;
				}else{
					thisObj._objects[i].KLOCSeated[x].setStroke(self.strokeColour);
				}

				thisObj._objects[i].KLOCSeated[x].KLOCCustomFont = false;
				canvas.renderAll();
				//Reset boundary coordinates
			}
		}
		allGroups.setBoundaryCoords(thisObj);
		allGroups.setBoundaryShuffle(null,thisObj);
	//Reset all objects assigned to a complex object
	}else if (thisObj._objects !== undefined && thisObj.KLOCComplexParent !== undefined){
		for (a=0; a<thisObj.KLOCComplexParent.$KLOCComplexObject.length; a++){
			for (b=1; b<thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects.length;b++){
				for (c=0; c<thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated.length; c++){

					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setFontSize(self.fontSize);
					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setLineHeight(self.lineHeight);
					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setOpacity(self.opacity);
					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontFamily = self.fontFamily;
					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setColor(self.fontColour);
					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setTextBackgroundColor(self.fontBackground);
					if (self.fontStyle == 'bold'){
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setFontWeight('bold');
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontStyle;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].textDecoration;
					}else if (self.fontStyle == 'italic'){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontWeight;
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setFontStyle('italic');
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].textDecoration;
					}else if (self.fontStyle == 'normal'){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontWeight;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontStyle;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].textDecoration;
					}else if (self.fontStyle == 'linethrough'){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontWeight;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontStyle;
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setTextDecoration('line-through');
					}else if (self.fontStyle == 'underline'){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontWeight;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontStyle;
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setTextDecoration('underline');
					}else if (self.fontStyle == 'overline'){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontWeight;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].fontStyle;
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setTextDecoration('overline');
					}
					if (self.stroke == "0" || self.stroke === 0 || self.stroke === ''){
						//thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setStroke("0");
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].strokeWidth;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].stroke;
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setStrokeWidth(0);
					}else{
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setStrokeWidth(self.strokeWidth);
					}

					if (thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].strokeColour === ''){
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].strokeWidth;
						delete thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].stroke;
					}else{
						thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].setStroke(self.strokeColour);
					}

					thisObj.KLOCComplexParent.$KLOCComplexObject[a]._objects[b].KLOCSeated[c].KLOCCustomFont = false;
					canvas.renderAll();
				}
			}
		}
	}else{
		thisObj.setFontSize(self.fontSize);
		thisObj.setLineHeight(self.lineHeight);
		thisObj.setOpacity(self.opacity);
		thisObj.fontFamily = self.fontFamily;
		thisObj.setColor(self.fontColour);
		thisObj.setTextBackgroundColor(self.fontBackground);
		if (self.fontStyle == 'bold'){
			thisObj.setFontWeight('bold');
			delete thisObj.fontStyle;
			delete thisObj.textDecoration;
		}else if (self.fontStyle == 'italic'){
			delete thisObj.fontWeight;
			thisObj.setFontStyle('italic');
			delete thisObj.textDecoration;
		}else if (self.fontStyle == 'normal'){
			delete thisObj.fontWeight;
			delete thisObj.fontStyle;
			delete thisObj.textDecoration;
		}else if (self.fontStyle == 'linethrough'){
			delete thisObj.fontWeight;
			delete thisObj.fontStyle;
			thisObj.setTextDecoration('line-through');
		}else if (self.fontStyle == 'underline'){
			delete thisObj.fontWeight;
			delete thisObj.fontStyle;
			thisObj.setTextDecoration('underline');
		}else if (self.fontStyle == 'overline'){
			delete thisObj.fontWeight;
			delete thisObj.fontStyle;
			thisObj.setTextDecoration('overline');
		}
		if (self.strokeWidth === '' || self.strokeWidth === undefined){
			thisObj.setStrokeWidth(0);
		}else{
			thisObj.setStrokeWidth(self.strokeWidth);
		}
		thisObj.setStroke(self.strokeColour);

		thisObj.KLOCCustomFont = false;
		canvas.renderAll();
	}
}

/*
 ██████  ███████ ████████      ██████  ██       ██████  ██████   █████  ██
██       ██         ██        ██       ██      ██    ██ ██   ██ ██   ██ ██
██   ███ █████      ██        ██   ███ ██      ██    ██ ██████  ███████ ██
██    ██ ██         ██        ██    ██ ██      ██    ██ ██   ██ ██   ██ ██
 ██████  ███████    ██         ██████  ███████  ██████  ██████  ██   ██ ███████
*/
FontsStyle.prototype.getGlobalFontProperties = function(property){
	var self = this;

	$('.global-fontsize').val(self.fontSize);
	$('.global-lineheight').val(self.lineHeight);
	$('.global-opacity').val(self.opacity );
	$('.global-strokewidth').val(self.strokeWidth);
	$('.global-fontfamily').val(self.fontFamily).prop('selected', true);
	$('.global-font-colour').val(self.fontColour);
	$('.global-font-backgroundcolour').val(self.fontBackground);
	$('.global-font-strokecolour').val(self.strokeColour);
	$('.global-font-style').val(self.fontStyle);
};
/*
███████ ███████ ████████      ██████  ██       ██████  ██████   █████  ██
██      ██         ██        ██       ██      ██    ██ ██   ██ ██   ██ ██
███████ █████      ██        ██   ███ ██      ██    ██ ██████  ███████ ██
     ██ ██         ██        ██    ██ ██      ██    ██ ██   ██ ██   ██ ██
███████ ███████    ██         ██████  ███████  ██████  ██████  ██   ██ ███████
*/
FontsStyle.prototype.setGlobalFontProperties = function(passedIn){
	var self = this;
	console.log('FONTS | setGlobalFontProperties');

	//LOOP THROUGH ALL CANVASTEXTOBJS AND UPDATE THEM WITH NEW PROPERTY
	for (i=0; i<canvasText.length; i++){
		if (canvasText[i].KLOCCustomFont !== true){
			self.fontSize = $('.global-fontsize').val();
			canvasText[i].setFontSize(self.fontSize);

			self.lineHeight = $('.global-lineheight').val();
			canvasText[i].setLineHeight(self.lineHeight);

			self.opacity = $('.global-opacity').val();
			canvasText[i].setOpacity(self.opacity);

			self.fontFamily = $('.global-fontfamily').val();
			canvasText[i].fontFamily = self.fontFamily;

			self.fontColour =  $('.global-fontcolour').val();
			canvasText[i].setColor(self.fontColour);

			self.fontBackground = $('.global-fontbackground').val();
			canvasText[i].setTextBackgroundColor(self.fontBackground);

			self.fontStyle = $('.global-font-style').val();

			if (self.fontStyle == 'bold'){
				canvasText[i].setFontWeight('bold');
				delete canvasText[i].fontStyle;
				delete canvasText[i].textDecoration;
			}else if (self.fontStyle == 'italic'){
				delete canvasText[i].fontWeight;
				canvasText[i].setFontStyle('italic');
				delete canvasText[i].textDecoration;
			}else if (self.fontStyle == 'normal' || self.fontStyle === ''){
				delete canvasText[i].fontWeight;
				delete canvasText[i].fontStyle;
				delete canvasText[i].textDecoration;
			}else if (self.fontStyle == 'linethrough'){
				delete canvasText[i].fontWeight;
				delete canvasText[i].fontStyle;
				canvasText[i].setTextDecoration('line-through');
			}else if (self.fontStyle == 'underline'){
				delete canvasText[i].fontWeight;
				delete canvasText[i].fontStyle;
				canvasText[i].setTextDecoration('underline');
			}else if (self.fontStyle == 'overline'){
				delete canvasText[i].fontWeight;
				delete canvasText[i].fontStyle;
				canvasText[i].setTextDecoration('overline');
			}
			if ($('.global-fontstroke').hasClass('delete-global-fill') >0){
				delete canvasText[i].stroke;
				$('.global-fontstroke').removeClass('delete-global-fill');
				$('.global-fontstroke').val('#ffffff');
			}else{
				self.strokeColour = $('.global-fontstroke').val();
				canvasText[i].setStroke(self.strokeColour);
			}

			if ($('.global-strokewidth').val() == '0'){
				self.strokeWidth = '';
				delete canvasText[i].strokeWidth;
				delete canvasText[i].stroke;
			}else{
				self.strokeWidth = $('.global-strokewidth').val();
				canvasText[i].setStrokeWidth(self.strokeWidth);
			}

		}
	}
	allGroups.updateAllGroups();
	//Render to canvas
	canvas.renderAll();
	//Reset boundary coordinates

};
//SET TEXT PROPERTIES ON 1 OBJECT
FontsStyle.prototype.getAndSetGroupPropertiesOnSingleObject = function(targetObj,groupObj){
	var self = this;
	console.info('FONTS | getAndSetGroupPropertiesOnSingleObject')

	//If Group has custom fonts attached to it already
	if (groupObj.KLOCFontPresets != undefined && groupObj.KLOCComplexObjectGroup == undefined){
		console.log('1')
		targetObj.setFontSize(groupObj.KLOCFontPresets.fontsize);
		targetObj.setLineHeight(groupObj.KLOCFontPresets.fontlineheight);
		targetObj.setOpacity(groupObj.KLOCFontPresets.fontopacity);
		targetObj.setFontFamily(groupObj.KLOCFontPresets.fontfamily);
		targetObj.setColor(groupObj.KLOCFontPresets.fontcolour);
		targetObj.setTextBackgroundColor(groupObj.KLOCFontPresets.fontbackground);
		targetObj.setStroke(groupObj.KLOCFontPresets.stokecolour);
		console.log(groupObj.KLOCFontPresets.fontstrokewidth)
		if (groupObj.KLOCFontPresets.fontstrokewidth === undefined || groupObj.KLOCFontPresets.fontstrokewidth === ''){
			targetObj.setStrokeWidth(0);
		}else{
			targetObj.setStrokeWidth(groupObj.KLOCFontPresets.fontstrokewidth);
		}
		if (groupObj.KLOCFontPresets.fontstyle == 'bold'){
			targetObj.setFontWeight('bold');
		}else if (groupObj.KLOCFontPresets.fontstyle == 'italic'){
			targetObj.setFontStyle('italic');
		}else if (groupObj.KLOCFontPresets.fontstyle == 'normal'){
			targetObj.setFontStyle('normal');
		}else if (groupObj.KLOCFontPresets.fontstyle == 'linethrough'){
			targetObj.setTextDecoration('linethrough');
		}else if (groupObj.KLOCFontPresets.fontstyle == 'underline'){
			targetObj.setTextDecoration('underline');
		}else if (groupObj.KLOCFontPresets.fontstyle == 'overline'){
			targetObj.setTextDecoration('overline');
		}
		targetObj.KLOCCustomFont = true;
	//If group has custom properties and is a complex object
	}else if (groupObj.KLOCComplexObjectGroup != undefined && groupObj.KLOCComplexParent.KLOCFontPresets.length != 0){
		console.log('2')
		targetObj.setFontSize(groupObj.KLOCComplexParent.KLOCFontPresets.fontsize);
		targetObj.setLineHeight(groupObj.KLOCComplexParent.KLOCFontPresets.fontlineheight);
		targetObj.setOpacity(groupObj.KLOCComplexParent.KLOCFontPresets.fontopacity);
		targetObj.setFontFamily(groupObj.KLOCComplexParent.KLOCFontPresets.fontfamily);
		targetObj.setColor(groupObj.KLOCComplexParent.KLOCFontPresets.fontcolour);
		targetObj.setTextBackgroundColor(groupObj.KLOCComplexParent.KLOCFontPresets.fontbackground);
		targetObj.setStroke(groupObj.KLOCComplexParent.KLOCFontPresets.stokecolour);
		if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstrokewidth === undefined || groupObj.KLOCComplexParent.KLOCFontPresets.fontstrokewidth === '' ){
			targetObj.setStrokeWidth(0);
		}else{
			targetObj.setStrokeWidth(groupObj.KLOCComplexParent.KLOCFontPresets.fontstrokewidth);
		}

		if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'bold'){
			targetObj.setFontWeight('bold');
		}else if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'italic'){
			targetObj.setFontStyle('italic');
		}else if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'normal'){
			targetObj.setFontStyle('normal');
		}else if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'linethrough'){
			targetObj.setTextDecoration('linethrough');
		}else if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'underline'){
			targetObj.setTextDecoration('underline');
		}else if (groupObj.KLOCComplexParent.KLOCFontPresets.fontstyle == 'overline'){
			targetObj.setTextDecoration('overline');
		}
		targetObj.KLOCCustomFont = true;
	//If Group has no custom properties (therefore is using the global font properties)
	}else{
		console.log('3')
		targetObj.setFontSize(self.fontSize);
		targetObj.setLineHeight(self.lineHeight);
		targetObj.setOpacity(self.opacity);
		targetObj.setFontFamily(self.fontFamily);
		targetObj.setColor(self.fontColour);
		targetObj.setTextBackgroundColor(self.fontBackground);
		targetObj.setStroke(self.strokeColour);

		console.log(self.strokeWidth)
		if (self.strokeWidth === undefined || self.strokeWidth === ''){
			targetObj.setStrokeWidth(0);
		}else{
			targetObj.setStrokeWidth(self.strokeWidth);
		}
		if (self.fontStyle == 'bold'){
			targetObj.setFontWeight('bold');
		}else if (self.fontStyle == 'italic'){
			targetObj.setFontStyle('italic');
		}else if (self.fontStyle == 'normal'){
			targetObj.setFontStyle('normal');
		}else if (self.fontStyle == 'linethrough'){
			targetObj.setTextDecoration('linethrough');
		}else if (self.fontStyle == 'underline'){
			targetObj.setTextDecoration('underline');
		}else if (self.fontStyle == 'overline'){
			targetObj.setTextDecoration('overline');
		}
	}
	//Reset boundary coordinates
	allGroups.setBoundaryCoords(groupObj);
}
//SET TEXT PROPERTIES ON 1 OBJECT
FontsStyle.prototype.setSingleFontProperties = function(property){
	var self = this;
	var trigger = property.trigger;
	var value = property.value;
	var object = property;
	var thisObj = rightClickState.targetObject;

	//console.log(property);

	function setFontPropertiesOnAGroup(passedObj){
		//console.log(passedObj)
		for (i=1; i<passedObj._objects.length; i++){
			//for each boundary
			for (x=0; x<passedObj._objects[i].KLOCSeated.length; x++){

				//set the flag on each text item inside a boundary to say it's been given a custom font proeprty
				for (a=0; a<canvasText.length; a++){
					if (canvasText[a] == passedObj._objects[i].KLOCSeated[x]){
						canvasText[a].KLOCCustomFont = true;
					}
				}

				passedObj._objects[i].KLOCSeated[x].setFontSize($('.single-fontsize').val());
				passedObj.KLOCFontPresets.fontsize = $('.single-fontsize').val();
				passedObj._objects[i].KLOCSeated[x].setLineHeight($('.single-lineheight').val());
				passedObj.KLOCFontPresets.fontlineheight = $('.single-lineheight').val();
				passedObj._objects[i].KLOCSeated[x].setOpacity($('.single-opacity').val());
				passedObj.KLOCFontPresets.fontopacity = $('.single-opacity').val();
				passedObj._objects[i].KLOCSeated[x].fontFamily = $('.single-fontfamily').val();
				passedObj.KLOCFontPresets.fontfamily = $('.single-fontfamily').val();
				passedObj._objects[i].KLOCSeated[x].setColor($('.single-font-colour').val());
				passedObj.KLOCFontPresets.fontcolour = $('.single-font-colour').val();
				passedObj._objects[i].KLOCSeated[x].setTextBackgroundColor($('.single-font-backgroundcolour').val());
				passedObj.KLOCFontPresets.fontbackground = $('.single-font-backgroundcolour').val();

				if ($('.single-fontstyle').val() == 'bold'){
					passedObj._objects[i].KLOCSeated[x].setFontWeight('bold');
					delete passedObj._objects[i].KLOCSeated[x].fontStyle;
					delete passedObj._objects[i].KLOCSeated[x].textDecoration;
				}else if ($('.single-fontstyle').val() == 'italic'){
					delete passedObj._objects[i].KLOCSeated[x].fontWeight;
					passedObj._objects[i].KLOCSeated[x].setFontStyle('italic');
					delete passedObj._objects[i].KLOCSeated[x].textDecoration;
				}else if ($('.single-fontstyle').val() == 'normal' || $('.single-fontstyle').val() === ''){
					delete passedObj._objects[i].KLOCSeated[x].fontWeight;
					delete passedObj._objects[i].KLOCSeated[x].fontStyle;
					delete passedObj._objects[i].KLOCSeated[x].textDecoration;
				}else if ($('.single-fontstyle').val() == 'linethrough'){
					delete passedObj._objects[i].KLOCSeated[x].fontWeight;
					delete passedObj._objects[i].KLOCSeated[x].fontStyle;
					passedObj._objects[i].KLOCSeated[x].setTextDecoration('line-through');
				}else if ($('.single-fontstyle').val() == 'underline'){
					delete passedObj._objects[i].KLOCSeated[x].fontWeight;
					delete passedObj._objects[i].KLOCSeated[x].fontStyle;
					passedObj._objects[i].KLOCSeated[x].setTextDecoration('underline');
				}else if ($('.single-fontstyle').val() == 'overline'){
					delete passedObj._objects[i].KLOCSeated[x].fontWeight;
					delete passedObj._objects[i].KLOCSeated[x].fontStyle;
					passedObj._objects[i].KLOCSeated[x].setTextDecoration('overline');
				}
				passedObj.KLOCFontPresets.fontstyle = $('.single-fontstyle').val();

				if ($('.single-font-strokecolour').hasClass('delete-single-fill') > 0){
					$('.single-font-strokecolour').removeClass('delete-single-fill');
					delete passedObj.KLOCFontPresets.strokecolour;
				}else{
					passedObj._objects[i].KLOCSeated[x].setStroke($('.single-font-strokecolour').val());
					passedObj.KLOCFontPresets.strokecolour = $('.single-font-strokecolour').val();
				}

				if ($('.single-strokewidth').val() == '0' || $('.single-strokewidth').val() === ''){
					delete passedObj.KLOCFontPresets.strokecolour;
					delete passedObj.KLOCFontPresets.fontstrokewidth;
				}else{
					passedObj._objects[i].KLOCSeated[x].setStrokeWidth($('.single-strokewidth').val());
					passedObj.KLOCFontPresets.fontstrokewidth = $('.single-strokewidth').val();
				}



			}
		}
	}

	function setFontPropertiesOnASingleObject(passedObj){
		//Not a group, likely a single text object
		passedObj.setFontSize($('.single-fontsize').val());
		passedObj.setLineHeight($('.single-lineheight').val());
		passedObj.setOpacity($('.single-opacity').val());
		passedObj.fontFamily = $('.single-fontfamily').val();
		passedObj.setColor($('.single-font-colour').val());
		passedObj.setTextBackgroundColor($('.single-font-backgroundcolour').val());
		if ($('.single-fontstyle').val() == 'bold'){
			passedObj.setFontWeight('bold');
			delete passedObj.fontStyle;
			delete passedObj.textDecoration;
		}else if ($('.single-fontstyle').val() == 'italic'){
			delete passedObj.fontWeight;
			passedObj.setFontStyle('italic');
			delete passedObj.textDecoration;
		}else if ($('.single-fontstyle').val() == 'normal' || $('.single-fontstyle').val() === ''){
			delete passedObj.fontWeight;
			delete passedObj.fontStyle;
			delete passedObj.textDecoration;
		}else if ($('.single-fontstyle').val() == 'linethrough'){
			delete passedObj.fontWeight;
			delete passedObj.fontStyle;
			passedObj.setTextDecoration('line-through');
		}else if ($('.single-fontstyle').val() == 'underline'){
			delete passedObj.fontWeight;
			delete passedObj.fontStyle;
			passedObj.setTextDecoration('underline');
		}else if ($('.single-fontstyle').val() == 'overline'){
			delete passedObj.fontWeight;
			delete passedObj.fontStyle;
			passedObj.setTextDecoration('overline');
		}
		if (rightClickState.targetObject.strokeColour === undefined && $('.single-font-strokecolour').val() == '#000000'){
			passedObj.setStroke('');
		}else{
			passedObj.setStroke($('.single-font-strokecolour').val());
		}
		passedObj.setStrokeWidth($('.single-strokewidth').val());
	}

	function setFontPropertiesOnAComplexGroup(passedObj){
		//console.log('passedObj',passedObj)
		for (x=0; x<passedObj.$KLOCComplexObject.length; x++){
			for (y=1; y<passedObj.$KLOCComplexObject[x]._objects.length; y++){
				for (z=0; z<passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated.length; z++){
					//for each text object attached to a complex object boundary

					for (a=0; a<canvasText.length; a++){
						if (canvasText[a] == passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z]){
							canvasText[a].KLOCCustomFont = true;
						}
					}

					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setFontSize($('.single-fontsize').val());
					passedObj.KLOCFontPresets.fontsize = $('.single-fontsize').val();
					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setLineHeight($('.single-lineheight').val());
					passedObj.KLOCFontPresets.fontlineheight = $('.single-lineheight').val();
					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setOpacity($('.single-opacity').val());
					passedObj.KLOCFontPresets.fontopacity = $('.single-opacity').val();
					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontFamily = $('.single-fontfamily').val();
					passedObj.KLOCFontPresets.fontfamily = $('.single-fontfamily').val();
					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setColor($('.single-font-colour').val());
					passedObj.KLOCFontPresets.fontcolour = $('.single-font-colour').val();
					passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setTextBackgroundColor($('.single-font-backgroundcolour').val());
					passedObj.KLOCFontPresets.fontbackground = $('.single-font-backgroundcolour').val();
					if ($('.single-fontstyle').val() == 'bold'){
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setFontWeight('bold');
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontStyle;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].textDecoration;
					}else if ($('.single-fontstyle').val() == 'italic'){
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontWeight;
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setFontStyle('italic');
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].textDecoration;
					}else if ($('.single-fontstyle').val() == 'normal' || $('.single-fontstyle').val() === ''){
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontWeight;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontStyle;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].textDecoration;
					}else if ($('.single-fontstyle').val() == 'linethrough'){
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontWeight;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontStyle;
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setTextDecoration('line-through');
					}else if ($('.single-fontstyle').val() == 'underline'){
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontWeight;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontStyle;
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setTextDecoration('underline');
					}else if ($('.single-fontstyle').val() == 'overline'){
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontWeight;
						delete passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].fontStyle;
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setTextDecoration('overline');
					}
					passedObj.KLOCFontPresets.fontstyle = $('.single-fontstyle').val();

					if ($('.single-font-strokecolour').hasClass('delete-single-fill') > 0){
						$('.single-font-strokecolour').removeClass('delete-single-fill');
						delete passedObj.KLOCFontPresets.strokecolour;
					}else{
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setStroke($('.single-font-strokecolour').val());
						passedObj.KLOCFontPresets.strokecolour = $('.single-font-strokecolour').val();
					}
					console.warn($('.single-strokewidth').val())
					if ($('.single-strokewidth').val() == '0' || $('.single-strokewidth').val() === '' || $('.single-strokewidth').val() === 0){
						delete passedObj.KLOCFontPresets.strokecolour;
						delete passedObj.KLOCFontPresets.fontstrokewidth;
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setStrokeWidth(0);
					}else{
						passedObj.$KLOCComplexObject[x]._objects[y].KLOCSeated[z].setStrokeWidth($('.single-strokewidth').val());
						passedObj.KLOCFontPresets.fontstrokewidth = $('.single-strokewidth').val();
					}

				}
			}
		}
	}

	//Set font properties on a root object of a complex obj
	if (thisObj.KLOCComplexObjectGroup !== undefined){
		console.warn('ting properties on complex font ');
		for (ab=0; ab<canvasObjects.length; ab++){
			if (canvasObjects[ab].$KLOCComplexObject !== undefined){
				for (abc=0; abc<canvasObjects[ab].$KLOCComplexObject.length; abc++){
					if (thisObj == canvasObjects[ab].$KLOCComplexObject[abc]){
						setFontPropertiesOnAComplexGroup(canvasObjects[ab]);
						break;
					}
				}
			}
		}
	//Are we inside a group?
	}else if (thisObj._objects !== undefined && thisObj.KLOCComplexObjectGroup === undefined){
		setFontPropertiesOnAGroup(rightClickState.targetObject);
	//Are we targeting a single lone text item?
	}else if (thisObj._objects === undefined && thisObj.KLOCComplexObjectGroup === undefined){
		setFontPropertiesOnASingleObject(rightClickState.targetObject);
	//none of the above
	}else{
		console.error('Error: Font properties target is invalid somehow...');
	}
	rightClickState.targetObject.KLOCCustomFont = true;

	//Render to canvas
	canvas.renderAll();
	//Reset boundary coordinates
	allGroups.setBoundaryCoords(rightClickState.targetObject);
	allGroups.setBoundaryShuffle(null,rightClickState.targetObject);
};
