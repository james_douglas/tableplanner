/* KLOC ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SITE: NAME OF SITE //////////////////////////////////////////////////////////////////////////////////////////////////////
// DEV: JAMES DOUGLAS //////////////////////////////////////////////////////////////////////////////////////////////////////
// BUILD: 2017 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/

$(document).ready(function(){


	// $('.ungroup').click(function(){
	// 	canvas.clear().renderAll();
	// })

	$('.create-table').click(function(){
		var tableType = $(this).attr('data-tabletype');
		closeMenu();
		if (tableType == 'RectangleTable'){
			canvasObjects.push(new RectangleTable({height: 200,width: 200},{top: 20,left: 20}));
		}else if(tableType == 'RectangleTableTopTable'){
			canvasObjects.push(new RectangleTableTopTable({height: 200,width: 200},{top: 20,left: 230}));
		}else if(tableType == 'CircleTable'){
			canvasObjects.push(new CircleTable({size: 200},{top:100,left: 100}));
		}else if(tableType == 'HorseshoeTable'){
			canvasObjects.push(new HorseshoeTable({size: 150},{top:100,left: 100}));
		}else{
			console.error('what');
		}
		//console.log(canvasObjects)
	});
	// $(window).click(function(){
	// 	closeMenu();
	// })

	/*
	███    ██  █████  ██    ██
	████   ██ ██   ██ ██    ██
	██ ██  ██ ███████ ██    ██
	██  ██ ██ ██   ██  ██  ██
	██   ████ ██   ██   ████
	*/

	function closeMenu(){
		$('.menu-parent-open').removeClass('menu-parent-open');
		$('.menu-grandparent-open').removeClass('menu-grandparent-open');
		$('.menu-parent').removeAttr('style');
		$('.menu-parent>i').removeClass('fa-times').addClass('fa-caret-down');
	}


	$('.menu-parent>a').click(function(e){
		//e.stopPropagation();

		if ($(this).parent().hasClass('menu-parent-open') === true){
			$('.menu-parent-open').removeClass('menu-parent-open');
			$('.menu-grandparent-open').removeClass('menu-grandparent-open');
			$(this).parent().find('i:first').removeClass('fa-times').addClass('fa-caret-down');
		}else{
			$('.menu-parent-open').removeClass('menu-parent-open');
			$('.menu-grandparent-open').removeClass('menu-grandparent-open');
			$(this).parent().addClass('menu-parent-open');
			$('.menu-parent>i').removeClass('fa-times').addClass('fa-caret-down');
			$(this).parent().find('i:first').addClass('fa-times').removeClass('fa-caret-down');
		}
	});

	$('.menu-manualclose').click(function(e){
		e.stopPropagation();
	});

	$('.menu-modal-close').click(function(){
		$('.menu-parent-open').removeClass('menu-parent-open');
		$('.menu-grandparent-open').removeClass('menu-grandparent-open');
	});

	$('.menu-grandparent>a').click(function(e){
		//e.stopPropagation();
		if ($(this).parent().hasClass('menu-grandparent-open') === true){
			$('.menu-grandparent-open').removeClass('menu-grandparent-open');
			$(this).parent().find('i:first').removeClass('fa-times').addClass('fa-caret-down');
		}else{
			$('.menu-grandparent-open').removeClass('menu-grandparent-open');
			$(this).parent().addClass('menu-grandparent-open');
			$('.menu-grandparent>i').removeClass('fa-times').addClass('fa-caret-down');
			$(this).parent().find('i:first').addClass('fa-times').removeClass('fa-caret-down');
		}
	});

	/*
	██████   █████   ██████  ███████ ████████  █████  ██████  ███████
	██   ██ ██   ██ ██       ██         ██    ██   ██ ██   ██ ██
	██████  ███████ ██   ███ █████      ██    ███████ ██████  ███████
	██      ██   ██ ██    ██ ██         ██    ██   ██ ██   ██      ██
	██      ██   ██  ██████  ███████    ██    ██   ██ ██████  ███████
	*/



	// $('.tp-pagetabs-tabcontent').each(function(){
	// 	$(this).css({height:($(window).height()-150)+'px',width:($(window).width()*0.75)+'px'})
	// })

	//$('.tp-diners-container').css({height:($(window).height()-150)+'px',width:($(window).width()*0.25)+'px'})

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
