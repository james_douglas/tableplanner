/*
 ██████  ██       ██████  ██████   █████  ██           ██████  ██████   ██████  ██    ██ ██████  ███████
██       ██      ██    ██ ██   ██ ██   ██ ██          ██       ██   ██ ██    ██ ██    ██ ██   ██ ██
██   ███ ██      ██    ██ ██████  ███████ ██          ██   ███ ██████  ██    ██ ██    ██ ██████  ███████
██    ██ ██      ██    ██ ██   ██ ██   ██ ██          ██    ██ ██   ██ ██    ██ ██    ██ ██           ██
 ██████  ███████  ██████  ██████  ██   ██ ███████      ██████  ██   ██  ██████   ██████  ██      ███████
KLOC 2017
DEV: James Douglas
*/

function Groups($passedin) {
	this.$globalObject = $passedin;
}

//Set boundary coordinates on all boundaries inside a group (needed to correct for boundary items inheriting coordinates from their parent group)
/*
███████ ███████ ████████     ██████   ██████  ██    ██ ███    ██ ██████   █████  ██████  ██    ██ ███████
██      ██         ██        ██   ██ ██    ██ ██    ██ ████   ██ ██   ██ ██   ██ ██   ██  ██  ██  ██
███████ █████      ██        ██████  ██    ██ ██    ██ ██ ██  ██ ██   ██ ███████ ██████    ████   ███████
     ██ ██         ██        ██   ██ ██    ██ ██    ██ ██  ██ ██ ██   ██ ██   ██ ██   ██    ██         ██
███████ ███████    ██        ██████   ██████   ██████  ██   ████ ██████  ██   ██ ██   ██    ██    ███████
*/


Groups.prototype.setBoundaryCoords = function(targetObj){
	var self = this;
	console.info('ALLGROUPS | setBoundaryCoords');

	for (a=0; a<canvasObjects.length; a++){
		if (canvasObjects[a].$group == targetObj){
			canvasObjects[a].destroyGhost(); //get rid of the ghost created on drag
			canvasObjects[a].calculateBoundaryCoords(targetObj);
		}else if (targetObj.KLOCComplexObjectGroup !== undefined && canvasObjects[a].$KLOCComplexObject !== undefined){
			canvasObjects[a].destroyGhost(); //get rid of the ghost created on drag
			canvasObjects[a].setKLOCComplexObjectPosition(targetObj);
			canvasObjects[a].calculateBoundaryCoords(targetObj);
		}else{
			//console.log('nope')
		}
	}
};

/*
██   ██ ██ ██      ██           ██████  ██   ██  ██████  ███████ ████████ ███████
██  ██  ██ ██      ██          ██       ██   ██ ██    ██ ██         ██    ██
█████   ██ ██      ██          ██   ███ ███████ ██    ██ ███████    ██    ███████
██  ██  ██ ██      ██          ██    ██ ██   ██ ██    ██      ██    ██         ██
██   ██ ██ ███████ ███████      ██████  ██   ██  ██████  ███████    ██    ███████
*/

Groups.prototype.ghostBuster = function(targetObj){
	var self = this;
	console.info('ALLGROUPS | ghostBuster');

	for (a=0; a<canvasObjects.length; a++){
		if (canvasObjects[a].$group == targetObj){
			canvasObjects[a].destroyGhost();
		}else if (targetObj.KLOCComplexObjectGroup !== undefined && canvasObjects[a].$KLOCComplexObject !== undefined){
			canvasObjects[a].destroyGhost();
		}else{
			//console.log('nope')
		}
	}
};


/*
███████ ███████ ████████      ██████  ██   ██  ██████  ███████ ████████
██      ██         ██        ██       ██   ██ ██    ██ ██         ██
███████ █████      ██        ██   ███ ███████ ██    ██ ███████    ██
     ██ ██         ██        ██    ██ ██   ██ ██    ██      ██    ██
███████ ███████    ██         ██████  ██   ██  ██████  ███████    ██
*/


//Creates a ghost of an object when it's moved
Groups.prototype.setGhost = function(targetObj){
	var self = this;

	for (j=0; j<canvasObjects.length; j++){
		if (canvasObjects[j].$group == targetObj){
			canvasObjects[j].createGhost(targetObj);
		}else if (canvasObjects[j] ==  targetObj.KLOCComplexParent){
			canvasObjects[j].createGhost(targetObj);
		}
	}
};

/*
███████ ██   ██ ██    ██ ███████ ███████ ██      ███████     ███████ ██ ███    ██  ██████  ██      ███████
██      ██   ██ ██    ██ ██      ██      ██      ██          ██      ██ ████   ██ ██       ██      ██
███████ ███████ ██    ██ █████   █████   ██      █████       ███████ ██ ██ ██  ██ ██   ███ ██      █████
     ██ ██   ██ ██    ██ ██      ██      ██      ██               ██ ██ ██  ██ ██ ██    ██ ██      ██
███████ ██   ██  ██████  ██      ██      ███████ ███████     ███████ ██ ██   ████  ██████  ███████ ███████
*/


Groups.prototype.setSingleBoundaryShuffle = function(targetObj,parentObj,errorCode){

};


/*
███████ ██   ██ ██    ██ ███████ ███████ ██      ███████      █████  ██      ██
██      ██   ██ ██    ██ ██      ██      ██      ██          ██   ██ ██      ██
███████ ███████ ██    ██ █████   █████   ██      █████       ███████ ██      ██
     ██ ██   ██ ██    ██ ██      ██      ██      ██          ██   ██ ██      ██
███████ ██   ██  ██████  ██      ██      ███████ ███████     ██   ██ ███████ ███████
*/


//Reshuffle items inside a boundary
Groups.prototype.setBoundaryShuffle = function(targetObj,parentObj,errorCode){
	var self = this;
	console.info('ALLGROUPS | setBoundaryShuffle');
	//RUN ON ALL BOUNDARIES ON A GIVEN OBJECT
	if (targetObj === null){
		for (jk=0; jk<canvasObjects.length; jk++){
			//If parent object matches an object in the canvasobjs
			if (canvasObjects[jk].$group == parentObj){
				for (kk=1;kk<parentObj._objects.length; kk++){
					canvasObjects[jk].reshuffleBoundaryItems(canvasObjects[jk].$group._objects[kk],parentObj);
				}
				break;
			//If parent object is in the array of complexobjs in the canvas obj..
			}else if($.inArray(parentObj, canvasObjects[jk].$KLOCComplexObject) !=  -1){
				for (kk=0;kk<canvasObjects[jk].$KLOCComplexObject.length;kk++){
					for (kkk=1;kkk<canvasObjects[jk].$KLOCComplexObject[kk]._objects.length;kkk++){
						canvasObjects[jk].reshuffleBoundaryItems(canvasObjects[jk].$KLOCComplexObject[kk]._objects[kkk],canvasObjects[jk].$KLOCComplexObject[kk]);
					}
				}
				break;
			}else{
				//console.error('ALLGROUPS | setBoundaryShuffle missmatch:',canvasObjects[j].$group,parentObj,errorCode);
			}
		}
	//RUN ON A SINGLE BOUNDARY
	}else{
		for (jk=0; jk<canvasObjects.length; jk++){
			if (canvasObjects[jk].$group == parentObj){
				canvasObjects[jk].reshuffleBoundaryItems(targetObj,parentObj);
				break;
			}else if($.inArray(parentObj, canvasObjects[jk].$KLOCComplexObject) !=  -1){
				canvasObjects[jk].reshuffleBoundaryItems(targetObj,parentObj);
				break;
			}else{
				//console.error('ALLGROUPS | setBoundaryShuffle missmatch:',canvasObjects[j].$group,parentObj,errorCode);
			}
		}
	}
};

/*
██    ██ ██████  ██████   █████  ████████ ███████      █████  ██      ██
██    ██ ██   ██ ██   ██ ██   ██    ██    ██          ██   ██ ██      ██
██    ██ ██████  ██   ██ ███████    ██    █████       ███████ ██      ██
██    ██ ██      ██   ██ ██   ██    ██    ██          ██   ██ ██      ██
 ██████  ██      ██████  ██   ██    ██    ███████     ██   ██ ███████ ███████
*/

//Reset all internal group positions and boundary calcs - triggered by setting font properties
//BUG THIS PROBABLY WON'T FIRE ON HORSEHOE
Groups.prototype.updateAllGroups = function(){
	var self = this;
	console.info('ALLGROUPS | updateAllGroups');


	for (a=0; a<canvasObjects.length; a++){
		if (canvasObjects[a].$KLOCComplexObject !== undefined){
			for (j=0;j<canvasObjects[a].$KLOCComplexObject.length;j++){

				canvasObjects[a].calculateBoundaryCoords(canvasObjects[a].$KLOCComplexObject[j]);
				canvasObjects[a].setKLOCComplexObjectPosition(canvasObjects[a].$KLOCComplexObject[j]);
				for (k=1;k<canvasObjects[a].$KLOCComplexObject[j]._objects.length;k++){
					//boundary, parent
					canvasObjects[a].reshuffleBoundaryItems(canvasObjects[a].$KLOCComplexObject[j]._objects[k],canvasObjects[a].$KLOCComplexObject[j]);
					break; //required to stop infinite loop nuking chrome - do not remove this.
				}
			}

		}else{
			canvasObjects[a].calculateBoundaryCoords(canvasObjects[a].$group);
			for (m=1; m<canvasObjects[a].$group._objects.length; m++){
				canvasObjects[a].reshuffleBoundaryItems(canvasObjects[a].$group._objects[m],canvasObjects[a].$group);
			}
		}
	}
};
