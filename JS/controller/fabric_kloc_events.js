/*
███████ ██    ██ ███████ ███    ██ ████████ ███████
██      ██    ██ ██      ████   ██    ██    ██
█████   ██    ██ █████   ██ ██  ██    ██    ███████
██       ██  ██  ██      ██  ██ ██    ██         ██
███████   ████   ███████ ██   ████    ██    ███████
KLOC 2017
DEV: James Douglas
*/

function Events(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change

	this.init();
	this.clickBindings();

	this.loadEventsList();
}

/*
██ ███    ██ ██ ████████
██ ████   ██ ██    ██
██ ██ ██  ██ ██    ██
██ ██  ██ ██ ██    ██
██ ██   ████ ██    ██
*/
Events.prototype.init = function(){
	var self = this;
};

/*
██       ██████   █████  ██████  ███████ ██    ██ ███████ ███    ██ ████████ ███████
██      ██    ██ ██   ██ ██   ██ ██      ██    ██ ██      ████   ██    ██    ██
██      ██    ██ ███████ ██   ██ █████   ██    ██ █████   ██ ██  ██    ██    ███████
██      ██    ██ ██   ██ ██   ██ ██       ██  ██  ██      ██  ██ ██    ██         ██
███████  ██████  ██   ██ ██████  ███████   ████   ███████ ██   ████    ██    ███████
*/
Events.prototype.loadEventsList = function(){
	var self = this;

	$.ajax({
		dataType: 'json',
		type:'GET',
		url:"./AJAX/event-list.php",
		data:"",
		success:function(data) {
			//console.log(data)

			for (i=0; i<data.events.length; i++){
				$('.events-list').append('<li><a class="events-list-singleevent" href="#" data-eventid="'+data.events[i].event_id+'">'+data.events[i].event_name+'</a></li>');
			}

			$('.events-list-singleevent').click(function(){
				$('.menu-parent-open').removeClass('menu-parent-open');
				$('.menu-grandparent-open').removeClass('menu-grandparent-open');
				self.loadSingleEvent($(this).attr('data-eventid'));
			});

		},
		error:function(data){
			console.error('Something\s wrong with something',data);
		}
	});
};

/*
██       ██████   █████  ██████  ███████ ██    ██ ███████ ███    ██ ████████
██      ██    ██ ██   ██ ██   ██ ██      ██    ██ ██      ████   ██    ██
██      ██    ██ ███████ ██   ██ █████   ██    ██ █████   ██ ██  ██    ██
██      ██    ██ ██   ██ ██   ██ ██       ██  ██  ██      ██  ██ ██    ██
███████  ██████  ██   ██ ██████  ███████   ████   ███████ ██   ████    ██
*/
Events.prototype.loadSingleEvent = function(eventID){
	var self = this;

	$.ajax({
		dataType: 'json',
		type:'GET',
		url:"./AJAX/event-single.php",
		data:{eventID : eventID},
		success:function(data) {
			//console.log(data)
			getDiners.getDiners(data);

		},
		error:function(data){
			console.error('Something\s wrong with something',data);
		}
	});
};


/*
 ██████ ██      ██  ██████ ██   ██ ██████  ██ ███    ██ ██████  ██ ███    ██  ██████  ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██       ██
██      ██      ██ ██      █████   ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███ ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██      ██
 ██████ ███████ ██  ██████ ██   ██ ██████  ██ ██   ████ ██████  ██ ██   ████  ██████  ███████
*/
Events.prototype.clickBindings = function(){
	var self = this;


};
