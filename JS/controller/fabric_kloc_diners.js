
/*
██████  ██ ███    ██ ███████ ██████  ███████
██   ██ ██ ████   ██ ██      ██   ██ ██
██   ██ ██ ██ ██  ██ █████   ██████  ███████
██   ██ ██ ██  ██ ██ ██      ██   ██      ██
██████  ██ ██   ████ ███████ ██   ██ ███████
KLOC 2017
DEV: James Douglas
*/

function Diners(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change

	this.dinersList = [];

	this.init();
	this.clickBindings();
}

Diners.prototype.init = function(){
	var self = this;

};



/*
██████  ███████ ███    ██ ██████  ███████ ██████
██   ██ ██      ████   ██ ██   ██ ██      ██   ██
██████  █████   ██ ██  ██ ██   ██ █████   ██████
██   ██ ██      ██  ██ ██ ██   ██ ██      ██   ██
██   ██ ███████ ██   ████ ██████  ███████ ██   ██
*/

Diners.prototype.renderToCanvas = function(obj){
	var self = this;

	var thisDinerName = obj.attr('data-name');
	var thisDinerSubtitle = obj.attr('data-subtitle');

	if (obj[0].classList == "canvas-diner-add"){
		//console.log('is parent');

		// if (obj[0].parentNode.childNodes[1].)
		// self.dinersList.push({KLOCDinerWithGuests:thisDinerName});


	}else if(obj[0].classList == "canvas-diner-guest-add"){
		// console.log('is child');
		// console.log('parent is...',obj[0].parentNode.parentNode.parentNode);
	}

	if (obj.hasClass('dinerAdded') === false){
		var positionLeft = 20;
		var positionTop = obj.offset().top-84;
		var newDiner = new fabric.Text(thisDinerName, {
			left: positionLeft,
			top: positionTop,
			fontWeight: '',
			textDecoration: '',
			KLOCDinerIsParent: false,
			stroke: globalFonts.strokeColour, //colour of text border
			strokeWidth: globalFonts.strokeWidth, //width of text border
			textBackgroundColor: globalFonts.fontBackground,
			fill: globalFonts.fontColour,
			lineHeight: globalFonts.lineHeight,
			fontSize: globalFonts.fontSize,
			fontFamily: globalFonts.fontFamily
		});
		canvas.add(newDiner);
		newDiner.hasControls = false;
		canvasText.push(newDiner);
		//console.log(newDiner)
		obj.addClass('dinerAdded');
	}else{
		console.log('Diner already on canvas');
	}
};

/*
 ██████ ██      ██  ██████ ██   ██     ██████  ██ ███    ██ ██████  ██ ███    ██  ██████  ███████
██      ██      ██ ██      ██  ██      ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██       ██
██      ██      ██ ██      █████       ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███ ███████
██      ██      ██ ██      ██  ██      ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██      ██
 ██████ ███████ ██  ██████ ██   ██     ██████  ██ ██   ████ ██████  ██ ██   ████  ██████  ███████
*/


Diners.prototype.clickBindings = function(){
	var self = this;

	var dinerFormGuests = 0;

	$(document).ready(function(){

		$(document).on('click','.canvas-diner-add',function(e){
			e.preventDefault();
			self.renderToCanvas($(this));
		});

		$('.get-diners-from-event').click(function(e){
			e.preventDefault();
		});

		$('.event-diners-close').click(function(){
			if ($(this).hasClass('diners-menu-closed') > 0){
				$(this).removeClass('diners-menu-closed');
				$(this).find('i').addClass('fa-times').removeClass('fa-align-justify');
				$('.tp-diners-container').removeClass('diners-menu-closed');
			}else{
				$(this).addClass('diners-menu-closed');
				$(this).find('i').removeClass('fa-times').addClass('fa-align-justify');
				$('.tp-diners-container').addClass('diners-menu-closed');
			}

		});

		$(document).on('click','.tp-diners-options-importall',function(e){
			e.preventDefault();
			$('.canvas-diner-add').each(function(){
				self.renderToCanvas($(this));
			});
			$('.canvas-diner-guest-add').each(function(){
				self.renderToCanvas($(this));
			});
		});

		$(document).on('click','.canvas-diner-guest-addall',function(e){
			e.preventDefault();
			$(this).addClass('dinerAdded');
			$(this).parent().parent().find('.canvas-diner-guest-add').each(function(){
				self.renderToCanvas($(this));
			});
		});

		$(document).on('click','.canvas-diner-guest-add',function(e){
			e.preventDefault();
			self.renderToCanvas($(this));
		});

		$('.dinerform-window-close').click(function(){
			$('.dinerform-window').toggle();
		});

		$(document).on('click','.dinerform-form-addguest', function(){
			$('.dinerform-form-guests').prepend('<label for="guest-firstname-'+dinerFormGuests+'" >Guest '+(dinerFormGuests+1)+' Name:</label><textarea name="guest-firstname-'+dinerFormGuests+'" placeholder="Guest"></textarea>');
			$('.dinerform-form-guests').addClass('dinerform-form-hasguests');
			dinerFormGuests++;
		});

		$(document).on('click','form.dinerform-form input[type=submit]', function(e){
			e.preventDefault();
			console.log();
			if ($('.dinerform-form-hasguests').length > 0){
				var toAppend = '';
				toAppend += '<li class="tp-diners-dinersParent"><a class="canvas-diner-add" href="#" data-name="'+$('.dinerform-form>textarea').val()+'" data-subtitle=""><p>'+$('.dinerform-form>textarea').val()+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a><div class="canvas-diner-guests-container"><span class="canvas-diner-guests-title">Guests</span><a class="canvas-diner-guest-addall"><span>Add all <i class="fa fa-plus" aria-hidden="true"></i></span></a></div><ul class="tp-diners-list-child">';

				$('.dinerform-form-guests textarea').each(function(){
					//self.renderToCanvas($(this));
					toAppend+='<li class="tp-diners-dinersChild"><a class="canvas-diner-guest-add" data-name="'+$(this).val()+'" data-subtitle="" href="#"><p>'+$(this).val()+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a></li>';
				});
				toAppend +='</ul></li>';
				$('.tp-diners-container .tp-diners-list').prepend(toAppend);
			}else{
				$('.tp-diners-container .tp-diners-list').prepend('<li class="tp-diners-dinersParent"><a class="canvas-diner-add" href="#" data-name="'+$('.dinerform-form>textarea').val()+'" data-subtitle=""><p>'+$('.dinerform-form>textarea').val()+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a></li>');
			}


			$('.tp-diners-container').addClass('diners-menu-open');
			$('.menu-parent-open').removeClass('menu-parent-open');
			$('.menu-grandparent-open').removeClass('menu-grandparent-open');
			$('.dinerform-window').toggle();
			$('.dinerform-form-guests').empty();
			$('.dinerform-form-hasguests').removeClass('dinerform-form-hasguests');
		});

		$(document).on('click','.manually-add-diner', function(e){
			e.preventDefault();
			$('.dinerform-form>textarea').val('');
			$('.dinerform-window').toggle();
		});
	});
};

/*
███    ███  █████  ███    ██ ██    ██  █████  ██      ██   ██    ██      █████  ██████  ██████
████  ████ ██   ██ ████   ██ ██    ██ ██   ██ ██      ██    ██  ██      ██   ██ ██   ██ ██   ██
██ ████ ██ ███████ ██ ██  ██ ██    ██ ███████ ██      ██     ████       ███████ ██   ██ ██   ██
██  ██  ██ ██   ██ ██  ██ ██ ██    ██ ██   ██ ██      ██      ██        ██   ██ ██   ██ ██   ██
██      ██ ██   ██ ██   ████  ██████  ██   ██ ███████ ███████ ██        ██   ██ ██████  ██████
*/

Diners.prototype.manuallyAdd = function(){
	var self = this;



};

/*
 █████       ██  █████  ██   ██     ██████  ██ ███    ██ ███████ ██████  ███████
██   ██      ██ ██   ██  ██ ██      ██   ██ ██ ████   ██ ██      ██   ██ ██
███████      ██ ███████   ███       ██   ██ ██ ██ ██  ██ █████   ██████  ███████
██   ██ ██   ██ ██   ██  ██ ██      ██   ██ ██ ██  ██ ██ ██      ██   ██      ██
██   ██  █████  ██   ██ ██   ██     ██████  ██ ██   ████ ███████ ██   ██ ███████
*/
Diners.prototype.getDiners = function(data){
	var self = this;

	console.log(data)
	var event = data.event;

	$('.tp-diners-container').addClass('diners-menu-open');
	$('.event-diners-title').html(event.event_name);
	$(this).parent().parent().parent().removeClass('menu-parent-open');
	console.log(event.diners.length)
	for (i=0; i<event.diners.length; i++){
		if (event.diners[i].guests !== undefined){
			console.log('i',i)
			var toAppend = '';
			toAppend += '<li class="tp-diners-dinersParent"><a class="canvas-diner-add" href="#" data-name="'+event.diners[i].name+'" data-subtitle="'+event.diners[i].subtitle+'"><p>'+event.diners[i].name+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a><div class="canvas-diner-guests-container"><span class="canvas-diner-guests-title">Guests</span><a class="canvas-diner-guest-addall"><span>Add all <i class="fa fa-plus" aria-hidden="true"></i></span></a></div><ul class="tp-diners-list-child">';

			for (j=0;j<event.diners[i].guests.length;j++){
				//console.log('j',j)
				toAppend+='<li class="tp-diners-dinersChild"><a class="canvas-diner-guest-add" data-name="'+event.diners[i].guests[j].name+'" data-subtitle="'+event.diners[i].guests[j].subtitle+'" href="#"><p>'+event.diners[i].guests[j].name+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a></li>';
			}
			toAppend +='</ul></li>';
			$('.tp-diners-container .tp-diners-list').prepend(toAppend);
		}else{
			//console.log('i',i)
			$('.tp-diners-container .tp-diners-list').prepend('<li class="tp-diners-dinersParent"><a class="canvas-diner-add" href="#" data-name="'+event.diners[i].name+'" data-subtitle="'+event.diners[i].subtitle+'"><p>'+event.diners[i].name+'</p><span>Add <i class="fa fa-plus" aria-hidden="true"></i></span></a></li>');
		}
	}

};

/*
 █████       ██  █████  ██   ██     ███████ ██    ██ ███████ ███    ██ ████████
██   ██      ██ ██   ██  ██ ██      ██      ██    ██ ██      ████   ██    ██
███████      ██ ███████   ███       █████   ██    ██ █████   ██ ██  ██    ██
██   ██ ██   ██ ██   ██  ██ ██      ██       ██  ██  ██      ██  ██ ██    ██
██   ██  █████  ██   ██ ██   ██     ███████   ████   ███████ ██   ████    ██
*/
Diners.prototype.getEvents = function(){

};
