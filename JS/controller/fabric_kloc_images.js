/*
██ ███    ███  █████   ██████  ███████ ███████
██ ████  ████ ██   ██ ██       ██      ██
██ ██ ████ ██ ███████ ██   ███ █████   ███████
██ ██  ██  ██ ██   ██ ██    ██ ██           ██
██ ██      ██ ██   ██  ██████  ███████ ███████
KLOC 2017
DEV: James Douglas
*/

function Images(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change

	this.init();
	this.clickBindings();
}

/*
██ ███    ██ ██ ████████
██ ████   ██ ██    ██
██ ██ ██  ██ ██    ██
██ ██  ██ ██ ██    ██
██ ██   ████ ██    ██
*/
Images.prototype.init = function(){
	var self = this;
}

/*
██ ███    ███ ██████   ██████  ██████  ████████
██ ████  ████ ██   ██ ██    ██ ██   ██    ██
██ ██ ████ ██ ██████  ██    ██ ██████     ██
██ ██  ██  ██ ██      ██    ██ ██   ██    ██
██ ██      ██ ██       ██████  ██   ██    ██
*/
Images.prototype.import = function(){
	var self = this;


}

/*
 ██████ ██      ██  ██████ ██   ██ ██████  ██ ███    ██ ██████  ██ ███    ██  ██████  ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██       ██
██      ██      ██ ██      █████   ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███ ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██      ██
 ██████ ███████ ██  ██████ ██   ██ ██████  ██ ██   ████ ██████  ██ ██   ████  ██████  ███████
*/
Images.prototype.clickBindings = function(){
	var self = this;


	$('.imageuploader-window-close').click(function(){
		$('.imageuploader-window').hide();
		$('.import-image-filepath').val('');
	});

	$('.import-image').click(function(){
		$('.imageuploader-window').show();
		//self.import();
	});

	$(document).on('change','.import-image-filepath',function(e){
		var reader = new FileReader();
		  reader.onload = function (event){
		    var imgObj = new Image();
		    imgObj.src = event.target.result;
		    imgObj.onload = function () {
		      var image = new fabric.Image(imgObj);
		      image.set({
		            angle: 0,
		            padding: 10,
		            cornersize:10,
		            height:110,
		            width:110,
		      });
		      canvas.centerObject(image);
		      canvas.add(image);
		      canvas.renderAll();
		 	};
		};
		$(document).on('click', '.import-image-file', function(){
			reader.readAsDataURL(e.target.files[0]);
		});
	});
};
