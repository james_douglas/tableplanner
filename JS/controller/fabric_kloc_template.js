/*
KLOC 2017
DEV: James Douglas
*/

function Events(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change

	this.init();
	this.clickBindings();
}

/*
██ ███    ██ ██ ████████
██ ████   ██ ██    ██
██ ██ ██  ██ ██    ██
██ ██  ██ ██ ██    ██
██ ██   ████ ██    ██
*/
Events.prototype.init = function(){
	var self = this;
}

/*
 ██████ ██      ██  ██████ ██   ██ ██████  ██ ███    ██ ██████  ██ ███    ██  ██████  ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██       ██
██      ██      ██ ██      █████   ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███ ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██      ██
 ██████ ███████ ██  ██████ ██   ██ ██████  ██ ██   ████ ██████  ██ ██   ████  ██████  ███████
*/
Events.prototype.clickBindings = function(){
	var self = this;
}
