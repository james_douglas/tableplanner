/*
 ██████  ██████       ██  ██████  ██████  ███    ██ ████████ ███████ ██   ██ ████████
██    ██ ██   ██      ██ ██      ██    ██ ████   ██    ██    ██       ██ ██     ██
██    ██ ██████       ██ ██      ██    ██ ██ ██  ██    ██    █████     ███      ██
██    ██ ██   ██ ██   ██ ██      ██    ██ ██  ██ ██    ██    ██       ██ ██     ██
 ██████  ██████   █████   ██████  ██████  ██   ████    ██    ███████ ██   ██    ██
KLOC 2017
DEV: James Douglas
*/

function ObjContext(object,property) {
	this.$object = object; //Passed in object to change font of
	this.$property = property; //Passed in property, property to change
	this.contextTarget = {};

	this.init();
}

/*
██ ███    ██ ██ ████████
██ ████   ██ ██    ██
██ ██ ██  ██ ██    ██
██ ██  ██ ██ ██    ██
██ ██   ████ ██    ██
*/
ObjContext.prototype.init = function(){
	var self = this;

};

/*
 ██████ ██      ██  ██████ ██   ██ ██████  ██ ███    ██ ██████  ██ ███    ██  ██████  ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ████   ██ ██   ██ ██ ████   ██ ██       ██
██      ██      ██ ██      █████   ██████  ██ ██ ██  ██ ██   ██ ██ ██ ██  ██ ██   ███ ███████
██      ██      ██ ██      ██  ██  ██   ██ ██ ██  ██ ██ ██   ██ ██ ██  ██ ██ ██    ██      ██
 ██████ ███████ ██  ██████ ██   ██ ██████  ██ ██   ████ ██████  ██ ██   ████  ██████  ███████
*/
ObjContext.prototype.clickBindings = function(){
	var self = this;

	//global font background colour
	$(document).on('click','.update-table',function(v){
		self.setObjectProperty();
	});

	$(document).on('click','.object-colour-reset',function(v){
		$(this).prev().addClass('delete-object-colour');
		self.setObjectProperty();
	});

	$('.delete-table').click(function(){
		var delObj = rightClickState.targetObject;
		canvas.remove(delObj);
		$('#rightclick-font-context').hide();

		//Loop through placeholders and delete them from both the object they're in, and the canvas
		for (x=0; x < delObj._objects.length; x++){
			for (y=0; y < delObj._objects[x].KLOCDinerPosition.length;y++){
				canvas.remove(delObj._objects[x].KLOCDinerPosition[y]);
			}
			delObj._objects[x].KLOCDinerPosition = [];
		}

		//Loop through diners, delete them from object, delete them and return them to the list
		for (z=1; z < delObj._objects.length; z++){
			for (aa=0; aa < delObj._objects[z].KLOCSeated.length; aa++){
				$('.dinerAdded').each(function(){
					if ($(this).attr('data-name') == delObj._objects[z].KLOCSeated[aa].text){
						$(this).removeClass('dinerAdded');
					}
				});
				canvas.remove(delObj._objects[z].KLOCSeated[aa]);
			}
		}

		//delete table from canvas list
		for (l=0; l<canvasObjects.length; l++){
			if (canvasObjects[l].$group == delObj){
				canvasObjects.splice(l, 1);
				break;
			}
		}
	});


};

/*
 ██████  ██████  ███    ██ ████████ ███████ ██   ██ ████████ ███    ███ ███████ ███    ██ ██    ██
██      ██    ██ ████   ██    ██    ██       ██ ██     ██    ████  ████ ██      ████   ██ ██    ██
██      ██    ██ ██ ██  ██    ██    █████     ███      ██    ██ ████ ██ █████   ██ ██  ██ ██    ██
██      ██    ██ ██  ██ ██    ██    ██       ██ ██     ██    ██  ██  ██ ██      ██  ██ ██ ██    ██
 ██████  ██████  ██   ████    ██    ███████ ██   ██    ██    ██      ██ ███████ ██   ████  ██████
*/


ObjContext.prototype.setContextMenu = function(obj){
	var self = this;
	self.contextTarget = obj;
	self.getProperties(self.contextTarget);
};

/*
 ██████  ███████ ████████     ██████  ██████   ██████  ██████  ███████ ██████  ████████ ██ ███████ ███████
██       ██         ██        ██   ██ ██   ██ ██    ██ ██   ██ ██      ██   ██    ██    ██ ██      ██
██   ███ █████      ██        ██████  ██████  ██    ██ ██████  █████   ██████     ██    ██ █████   ███████
██    ██ ██         ██        ██      ██   ██ ██    ██ ██      ██      ██   ██    ██    ██ ██           ██
 ██████  ███████    ██        ██      ██   ██  ██████  ██      ███████ ██   ██    ██    ██ ███████ ███████
*/


ObjContext.prototype.getProperties = function(obj){
	var self = this;
	$('.canvas-object-backgroundcolour').val(self.contextTarget._objects[0].getFill());
	$('.canvas-object-strokecolour').val(self.contextTarget._objects[0].getStroke());
	$('.canvas-object-tablewidth').val(Math.floor(obj.width*obj.scaleX));
	$('.canvas-object-tableheight').val(Math.floor(obj.height*obj.scaleY));
	if (self.contextTarget._objects[0].strokeWidth === 0 || self.contextTarget._objects[0].strokeWidth === '' || self.contextTarget._objects[0].strokeWidth === undefined){
		$('.canvas-object-strokewidth').val(0);
	}else{
		$('.canvas-object-strokewidth').val(self.contextTarget._objects[0].strokeWidth);
	}

	if (self.contextTarget._objects[0].stroke === '' || self.contextTarget._objects[0].stroke === undefined){
		$('.canvas-object-strokecolour').val(0);
	}else{
		$('.canvas-object-strokecolour').val(self.contextTarget._objects[0].stroke);
	}

	console.log(self.contextTarget._objects[0].opacity)
	if(self.contextTarget._objects[0].opacity === 0 || self.contextTarget._objects[0].opacity === undefined){
		$('.canvas-object-opacity').val(0);
	}else{
		$('.canvas-object-opacity').val(self.contextTarget._objects[0].opacity);
	}

	console.log(obj);

	self.clickBindings();
};

/*
███████ ███████ ████████     ██████  ██████   ██████  ██████  ███████ ██████  ████████ ██    ██
██      ██         ██        ██   ██ ██   ██ ██    ██ ██   ██ ██      ██   ██    ██     ██  ██
███████ █████      ██        ██████  ██████  ██    ██ ██████  █████   ██████     ██      ████
     ██ ██         ██        ██      ██   ██ ██    ██ ██      ██      ██   ██    ██       ██
███████ ███████    ██        ██      ██   ██  ██████  ██      ███████ ██   ██    ██       ██
*/
ObjContext.prototype.setObjectProperty = function(obj){
	var self = this;
	// console.log(obj);
	// console.log(self.contextTarget);
	console.log($('.canvas-object-strokewidth').val(),$('.canvas-object-strokecolour').val());

	console.log(self.contextTarget._objects[0].getFill())
	if ($('.canvas-object-backgroundcolour').hasClass('delete-object-colour') > 0){
		$('.delete-object-colour').removeClass('delete-object-colour');
		self.contextTarget._objects[0].setFill('#cccccc')
		$('.canvas-object-backgroundcolour').val('#cccccc')
	}else{
		self.contextTarget._objects[0].setFill($('.canvas-object-backgroundcolour').val());
	}

	if ($('.canvas-object-strokecolour').hasClass('delete-object-colour') > 0){
		$('.delete-object-colour').removeClass('delete-object-colour');
		self.contextTarget._objects[0].setFill('#cccccc');
		$('.canvas-object-strokecolour').val('#000000');
	}else{
		self.contextTarget._objects[0].setFill($('.canvas-object-backgroundcolour').val());
	}

	self.contextTarget._objects[0].setOpacity($('.canvas-object-opacity').val());

	//BUG - no idea why this won't work.
	// self.contextTarget._objects[0].setStrokeWidth($('.canvas-object-strokewidth').val());

	canvas.renderAll();
  
};
