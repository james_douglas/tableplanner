/*
 ██████  ██       ██████  ██████   █████  ██      ███████
██       ██      ██    ██ ██   ██ ██   ██ ██      ██
██   ███ ██      ██    ██ ██████  ███████ ██      ███████
██    ██ ██      ██    ██ ██   ██ ██   ██ ██           ██
 ██████  ███████  ██████  ██████  ██   ██ ███████ ███████
*/
var lockToAnObject = {};
var snapOrientation = '';
var targetIntersection = false;
var canvasObjects = []; //THIS SHOULD BE POPULATED WITH EVERY TABLE THAT'S ADDED TO THE PAGE
var canvasText = []; //THIS SHOULD BE POPULATED WITH EVERY TEXTOBJ THAT'S ADDED TO THE PAGE
var rightClickState = {state:false,left:0,top:0}; //THIS IS SET TO TRUE IF A VALID RIGHT CLICK OCCURS IN CANVAS

/*
██████  ███████ ███    ██ ██████  ███████ ██████           ██████  ██████       ██
██   ██ ██      ████   ██ ██   ██ ██      ██   ██         ██    ██ ██   ██      ██
██████  █████   ██ ██  ██ ██   ██ █████   ██████          ██    ██ ██████       ██
██   ██ ██      ██  ██ ██ ██   ██ ██      ██   ██         ██    ██ ██   ██ ██   ██
██   ██ ███████ ██   ████ ██████  ███████ ██   ██ ███████  ██████  ██████   █████
*/
var canvas = new fabric.Canvas('canvas');

//RENDERING MULTIPLE CANVASES
//http://fabricjs.com/many-objects

//DRAG AND DROP BETWEEN CANVASES
//http://stackoverflow.com/questions/21310843/how-to-drag-and-drop-between-canvases-in-fabric-js

$(document).ready(function(){
	$('.tp-pagetabs-tabcontent').each(function(){
		canvas.setHeight($(window).height()-80);
		canvas.setWidth($(window).width());
	});
});

/*
 ██████  ██       ██████  ██████   █████  ██               ██████  ██████       ██ ███████  ██████ ████████ ███████
██       ██      ██    ██ ██   ██ ██   ██ ██              ██    ██ ██   ██      ██ ██      ██         ██    ██
██   ███ ██      ██    ██ ██████  ███████ ██              ██    ██ ██████       ██ █████   ██         ██    ███████
██    ██ ██      ██    ██ ██   ██ ██   ██ ██              ██    ██ ██   ██ ██   ██ ██      ██         ██         ██
 ██████  ███████  ██████  ██████  ██   ██ ███████ ███████  ██████  ██████   █████  ███████  ██████    ██    ███████
*/

var globalFonts = new FontsStyle();	//Global fonts for canvas
var getDiners = new Diners();		//Load JSON to pull in diners
var allGroups = new Groups();		//Functions to run across groups (reshuffle, calc coordinates etc)
var loadEvents = new Events();		//loads the Ajax for pulling in events
var objContext = new ObjContext();	//functions for the object context menu
var images = new Images();			//Add custom images
var menu = new Menu();				//Add menu functionality

/*
███    ███  ██████  ██    ██ ███████ ███████ ███████ ████████  █████  ████████ ███████
████  ████ ██    ██ ██    ██ ██      ██      ██         ██    ██   ██    ██    ██
██ ████ ██ ██    ██ ██    ██ ███████ █████   ███████    ██    ███████    ██    █████
██  ██  ██ ██    ██ ██    ██      ██ ██           ██    ██    ██   ██    ██    ██
██      ██  ██████   ██████  ███████ ███████ ███████    ██    ██   ██    ██    ███████
*/
canvas.on({
	'mouse:down': mouseState('down'),
	'mouse:up': mouseState('up')
});

document.oncontextmenu = RightMouseDown;
function RightMouseDown(e) {
	//ENABLE RIGHT CLICK FUNCTIONALITY ON CANVAS
	if (e.srcElement.className == 'upper-canvas '){
		console.log(e);
		rightClickState.state = true;
		rightClickState.left = e.clientX;
		rightClickState.top = e.clientY;
		return false;
	}else{
		rightClickState.state = false;
		//console.log('nope')
	}
}
function mouseState(arg) {
	return function(e) {
		//console.clear()
		var parentObj = lockToAnObject.parent;
		var boundaryObj = lockToAnObject.child;

		if(e.target === null){
			console.error('e.target == NULL - canvas clicked without target');
		}else{
			//Reset Height and width
			console.log('e.target',e.target);
			//MAKE SURE WE'RE NOT INTERACTING WITH A GROUP
			if (e.target._objects === undefined){
				console.warn('MOUSE '+arg+' FIRING');

				//ON MOUSE UP EVENT (END CLICK)
				if (arg == 'up'){
					targetIntersection = false;

					if (rightClickState.state === true){
						console.log('RICHT CLICKCT');
						rightClickState.targetObject = e.target;

						$('.context-table-properties').hide();
						$('.context-font-properties').removeClass('col-md-6').addClass('col-md-12').removeClass('col-lg-6').addClass('col-lg-12')

						$('#rightclick-font-context').show();
						globalFonts.setContextMenu(e.target);
						rightClickState.state = false;
					}else{
						rightClickState.state = false;
					}

					//ATTACHING
					canvas.forEachObject(function(obj) {

						//make sure that the 2 objects that are intersecting AREN'T 2 text objects
						if (e.target.intersectsWithObject(obj) && obj._objects !== undefined){
							////////////////////////////////////////////////////////////////////
							//Set all boundaries to NO FILL
							////////////////////////////////////////////////////////////////////
							for (i=1; i<parentObj._objects.length; i++){
								parentObj._objects[i].setFill('');
							}

							////////////////////////////////////////////////////////////////////
							//MOVING BETWEEN 2 BOUNDARIES
							////////////////////////////////////////////////////////////////////
							if (e.target.KLOCLastIntersection != boundaryObj && e.target.KLOCLastIntersection !== undefined){
								console.info('moved between 2 boundaries');

								//Remove the textObj from it's previous boundary
								var index = e.target.KLOCLastIntersection.KLOCSeated.indexOf(e.target);
								e.target.KLOCLastIntersection.KLOCSeated.splice(index, 1);

								//Look through the array of tables to find the one we've collided with, then we can run its functions
								allGroups.setBoundaryShuffle(e.target.KLOCLastIntersection,parentObj);
							}else{
								//console.info('boundaries same or new boundary');
							}

							////////////////////////////////////////////////////////////////////
							//ADDING TEXTOBJ TO A BOUNDARY (AND ITS ARRAY)
							////////////////////////////////////////////////////////////////////
							//If not already in array
							if( $.inArray(e.target, boundaryObj.KLOCSeated) == -1){
								if(parentObj.KLOCTableType == 'CircleTable' || parentObj.KLOCTableType == 'bezier'){
									boundaryObj.setStroke('#A2D4AB');
								}else{
									boundaryObj.setFill('#A2D4AB');
								}


								//if the boundary is in a group that has custom font properties, set them on the text obj
								globalFonts.getAndSetGroupPropertiesOnSingleObject(e.target,obj);

								//If first item on a boundary add it, don't try and work out where to put it
								if ($.isEmptyObject(boundaryObj.KLOCSeated) === true){
									boundaryObj.KLOCSeated.push(e.target);
								//If not first, work out where to put it based on where its dropped
								}else{
									//For each textobj already at the boundary, see if this textobjs top is lower than that item
									//if it's lower than an item, ignore that one - keep going until an items got a higher top (further down page) than the one we're ADDING
									//insert the new textobj above the item that's got a lower top than it. That make sense?
									//same thing for horizontal, just not above or below...

									////////////////////////////////////////////////////////////////////
									//SET ORDER OF ITEMS ON A BOUNDARY
									////////////////////////////////////////////////////////////////////
									//BUG --  somehow dies usually when the text item locks to click and the mouse up doens't fire
									for (a=0; a < boundaryObj.KLOCSeated.length; a++){
										if (boundaryObj.KLOCOrientation == 'horizontal'){
											if (e.target.left > boundaryObj.KLOCSeated[a].left){
												if (a+1 == boundaryObj.KLOCSeated.length){
													boundaryObj.KLOCSeated.push(e.target);
													break;
												}
											}else{
												if (a === 0){
													boundaryObj.KLOCSeated.splice( 0, 0, e.target );
												}else{
													boundaryObj.KLOCSeated.splice( a, 0, e.target );
												}
												break;
											}
										}else{
											if (e.target.top > boundaryObj.KLOCSeated[a].top){
												if (a+1 == boundaryObj.KLOCSeated.length){
													boundaryObj.KLOCSeated.push(e.target);
													break;
												}
											}else{
												if (a === 0){
													boundaryObj.KLOCSeated.splice( 0, 0, e.target );
												}else{
													boundaryObj.KLOCSeated.splice( a, 0, e.target );
												}
												break;
											}
										}
									}
								}
								e.target.KLOCLastIntersection = boundaryObj;//NOTE:: assign boundary to text
								boundaryObj.KLOCTextHeight = e.target.height; //set the boundaries text height size (will be spacing per item in shuffle)

								//SET MAXIMUM NUMBER OF TEXT ITEMS PER BOUNDARY
								//if top table position
								if (boundaryObj.KLOCBoundaryName == 'top_c'){
									boundaryObj.KLOCMaxBoundarySeating = 1;
								//calculate from width instead of height
								}else{
									boundaryObj.KLOCMaxBoundarySeating = (parentObj.height*parentObj.scaleY) / e.target.height;
								}

							}else{
								console.error('Same e.target dropped onto boundary');
								boundaryObj.setFill('');
								boundaryObj.setStoke('');
							}

							//If they're are less textobjects than we have space for
							if (boundaryObj.KLOCSeated.length <= Math.floor(boundaryObj.KLOCMaxBoundarySeating)){
								//console.log('space for new elements')
								//Reset boundary coordinates
								allGroups.setBoundaryShuffle(boundaryObj,parentObj,257);
							//too many textobjects for the boundary side
							}else{
								console.error('THE NUMBER OF ITEMS IS TOO DAMNED HIGH');
							}
						}
					});
				//ON MOUSE DOWN EVENT (START CLICK)
				}else if (arg == 'down'){
					console.info('ITEM DETACHED');
					/*
					██████  ███████ ████████  █████   ██████ ██   ██
					██   ██ ██         ██    ██   ██ ██      ██   ██
					██   ██ █████      ██    ███████ ██      ███████
					██   ██ ██         ██    ██   ██ ██      ██   ██
					██████  ███████    ██    ██   ██  ██████ ██   ██
					*/
					//Find the text item inside the boundary array and REMOVE it when we're detaching
					if (e.target.KLOCLastIntersection !== undefined){
						if(parentObj.KLOCTableType == 'CircleTable' || parentObj.KLOCObjectType == 'bezier-curve'){
							e.target.KLOCLastIntersection.setStroke('purple');
						}else{
							e.target.KLOCLastIntersection.setFill('purple');
						}

						var index = e.target.KLOCLastIntersection.KLOCSeated.indexOf(e.target);
						e.target.KLOCLastIntersection.KLOCSeated.splice(index, 1);
						globalFonts.reassignGlobalFontProperties(e.target);
						e.target.setAngle(0); //reset angle of text if pulled from a boundary where it was previously rotated
						canvas.renderAll();

						//Reset boundary coordinates
						allGroups.setBoundaryShuffle(e.target.KLOCLastIntersection,parentObj,289);

						//make the detached object forget any intsections it previously had so we can treat its next intersection as a new event
						delete e.target.KLOCLastIntersection;
					}
				}else{

				}
			}else{
				//console.warn('Group object interaction')

				if (arg == 'up'){
					//Reset boundary coordinates
					console.log('mouse up');
					/*
					██████  ██  ██████  ██   ██ ████████  ██████ ██      ██  ██████ ██   ██
					██   ██ ██ ██       ██   ██    ██    ██      ██      ██ ██      ██  ██
					██████  ██ ██   ███ ███████    ██    ██      ██      ██ ██      █████
					██   ██ ██ ██    ██ ██   ██    ██    ██      ██      ██ ██      ██  ██
					██   ██ ██  ██████  ██   ██    ██     ██████ ███████ ██  ██████ ██   ██
					*/
					if (rightClickState.state === true){
						console.log('RICHT CLICKCT ON TABLESAFTEN');
						rightClickState.targetObject = e.target;
						$('#rightclick-font-context').show();
						$('.context-table-properties').show();
						$('.context-font-properties').removeClass('col-md-12').addClass('col-md-6').removeClass('col-lg-12').addClass('col-lg-6')
						objContext.setContextMenu(e.target);
						globalFonts.setContextMenu(e.target);


						rightClickState.state = false;
					}else{
						rightClickState.state = false;
						allGroups.setBoundaryCoords(e.target);
						allGroups.ghostBuster(e.target);
						allGroups.setBoundaryShuffle(null,e.target);
					}
				//Create ghost from drag on mouse down
				}else{
					//Remove boundary fill when object selected
					for (i=1; i<e.target._objects.length;i++){
						e.target._objects[i].setFill('');
					}
					allGroups.setGhost(e.target);
				}
			}
		}
	};
}

/*
 ██████  ██████  ██      ██      ██ ███████ ██  ██████  ███    ██ ███████
██      ██    ██ ██      ██      ██ ██      ██ ██    ██ ████   ██ ██
██      ██    ██ ██      ██      ██ ███████ ██ ██    ██ ██ ██  ██ ███████
██      ██    ██ ██      ██      ██      ██ ██ ██    ██ ██  ██ ██      ██
 ██████  ██████  ███████ ███████ ██ ███████ ██  ██████  ██   ████ ███████
*/

//On canvas action
canvas.on({
	'object:moving': onChange,
	'object:rotating': onChange,
});

function onChange(options) {
	//console.log(options)
	//console.log('onChange Firing')

	//////////////////////////////////////////////////////////////////////
	//NOTE -- MOVEMENT DETECTION
	//////////////////////////////////////////////////////////////////////

	//KLOCCOMPLEXGROUPS - calcualte ghost movement during drag
	for (l=0; l<canvasObjects.length; l++){
		if (options.target.KLOCComplexObjectGroup !== undefined){
			canvasObjects[l].calculateGhostMovement(options.target);
			break;
		}else{

		}
	}

	//Update the coordinates of the moved item
	options.target.setCoords();

	//Check to see if current canvas interacts with another canvas
	canvas.forEachObject(function(obj) {

		//Kill the for each loop if the current object === the item we're dragging
		if (obj === options.target) return;

		//////////////////////////////////////////////////////////////////////
		//NOTE -- COLLISION DETECTION
		//////////////////////////////////////////////////////////////////////
		//If the object we're dragging hits another object
		if (options.target.intersectsWithObject(obj)){
			//console.log('COLLISION WITH OBJECT')

			//COLLISION BETWEEN TEXT AND A GROUP
			if (obj._objects !== undefined){
				//console.log('IS GROUP');
				//Loop through canvas objects to compare each canvas obj to the obj being hit by the dragged item
				for (i=1; i<obj._objects.length; i++){

					//if dragged object intersects with the coordinates of a canvas object
					//NOTE - intersectsWithRect isn't 'rectangle' - it's intersects with 2 points (tl,br) so all boundaries
					//will have this, even if they're not a rectangle. Paths will also work with this.
					if (options.target.intersectsWithRect((obj._objects[i].aCoords.tl),(obj._objects[i].aCoords.br)) === true){
						//console.log('COLLISION WITH BOUNDARY',obj._objects[i].KLOCBoundaryName)
						//Error check to avoid collisions on object ghosts
						if (obj.KLOCGhostObject !== true){
							//Reset line hover effect on all other boundaries (stops double hovers & if the break stops fill being removed)
							//Otherwise the break can fire and leave a boundary highlighted
							for (j=1; j<obj._objects.length; j++){
								obj._objects[j].setFill('');
								obj._objects[j].setStroke('');
							}

							if (obj.KLOCTableType == 'CircleTable' || obj.KLOCTableType == 'bezier'){
								obj._objects[i].setStroke('#547A82'); //intersected boundary
							}else{
								obj._objects[i].setFill('#547A82'); //intersected boundary
							}

							lockToAnObject = {parent:obj,child:obj._objects[i]};
							break; //break for loop since we've found the boundary we care about
						}
					}else{
						//console.log('collision with something, not a boundary?',obj.KLOCTableType,i)
						if (obj.KLOCGhostObject !== true){
							obj._objects[i].setFill('');
							obj._objects[i].setStroke('');
						}
					}
				}

			//COLLISION BETWEEN TEXT AND SOMETHING ELSE
			}else{
				//console.log('WAT IS THIS IS NOT GROUP!?',obj)
			}

		}else{
			//console.log('no collision')
			//If no intersection, clear hover effects on each boundary
			if (obj._objects !== undefined && obj.KLOCGhostObject !== true){
				for (j=1; j<obj._objects.length; j++){
					obj._objects[j].setFill('');
					obj._objects[j].setStroke('');
				}
			}

		}
	});
}

/*
███████  ██████  █████  ██      ██ ███    ██  ██████
██      ██      ██   ██ ██      ██ ████   ██ ██
███████ ██      ███████ ██      ██ ██ ██  ██ ██   ███
     ██ ██      ██   ██ ██      ██ ██  ██ ██ ██    ██
███████  ██████ ██   ██ ███████ ██ ██   ████  ██████
*/
canvas.on({
	'object:scaling': onScale
});

function onScale(options) {
	//console.log('resizing')

	//////////////////////////////////////////////////////////////////////
	//NOTE -- MOVEMENT DETECTION
	//////////////////////////////////////////////////////////////////////
	//If we're moving a group, update the groups boundaries
	if (options.target._objects !== undefined){
		allGroups.setBoundaryCoords(options.target);
	}

	//Update the coordinates of the moved item
	options.target.setCoords();

}
